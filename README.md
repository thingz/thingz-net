Official thingz.NET development and issue tracking is done at https://gitlab.com/thingz/thingz-net.

# thingz.NET
thingz.NET is a .NET implementation of the thingz platform.

## What is thingz
thingz is a language-agnostic cross-platform concept for abstracting, modelling and automating hardware and software components.
Nodes running any implementation of thingz can be connected and thereby provide interoperability between platforms, operating systems and programming languages.
Configuration of even complex systems is heavily automated to always have a working setup out of the box, if possible.

## State of development
thingz and thingz.NET are still under heavy development and do not yet provide a stable API. Before finalizing the API the basic features for a working automation and configuration system will be implemented.

## License
thingz.NET is published under the [MIT License](LICENSE).
