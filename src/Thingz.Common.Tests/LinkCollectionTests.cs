﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Common.EventsArgs;
using Xunit;

namespace Thingz.Common.Tests
{
    public class LinkCollectionTests
    {
        [Fact]
        public void Empty_CreateLink_LinkAdded()
        {
            var name = "name";
            var collection = new LinkCollection<Entity>();
            collection.CreateLink(name);

            Assert.Contains(name, collection.Select(i => i.Key));
        }

        [Fact]
        public void NonEmpty_CreateDuplicateLink_Throws()
        {
            var name = "name";
            var collection = new LinkCollection<Entity>();
            collection.CreateLink(name);

            Assert.Throws<ArgumentException>(() =>
                collection.CreateLink(name));
        }

        [Fact]
        public void Empty_RemoveLink_Throws()
        {
            var collection = new LinkCollection<Entity>();

            Assert.Throws<KeyNotFoundException>(() =>
                collection.RemoveLink("name"));
        }

        [Fact]
        public void NonEmpty_LinkChangeTarget_RaisesLinkChanged()
        {
            var name = "name";
            var collection = new LinkCollection<Entity>();
            collection.CreateLink(name);

            Assert.Raises<TargetChangedArgs>(
                handler => collection.LinkChanged += handler,
                handler => collection.LinkChanged -= handler,
                () => collection[name].Value = new Entity());
        }
    }
}
