﻿using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.Internal;
using Xunit;

namespace Thingz.Common.Tests.Internal
{
    public class AbstractEntityContainerCollectionTests
    {
        [Fact]
        public void Constructor_WithParameters_CallsVirtualAdd()
        {
            var container = new Mock<AbstractEntityContainerCollection<AbstractEntityContainer>>(
                new List<KeyValuePair<string, AbstractEntityContainer>>() {
                    new KeyValuePair<string, AbstractEntityContainer>("key", null)
                });

            Assert.NotNull(container.Object);
            container.Protected().Verify("Add", Times.Once(), ItExpr.IsAny<string>(), ItExpr.IsAny<AbstractEntityContainer>());
        }
    }
}
