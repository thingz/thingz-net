﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Thingz.Common.EventsArgs;

namespace Thingz.Common.Tests.Internal
{
    public class AbstractEntityContainerTests
    {
        [Fact]
        public void ChangeValue_RaisesEvent()
        {
            var container = new MockEntityContainer("My.Type.Id");

            Assert.Raises<TargetChangedArgs>(
                handler => container.TargetChanged += handler,
                handler => container.TargetChanged -= handler,
                () => container.Value = new Entity());
        }
    }
}
