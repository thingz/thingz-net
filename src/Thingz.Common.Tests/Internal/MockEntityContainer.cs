﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.Internal;

namespace Thingz.Common.Tests.Internal
{
    class MockEntityContainer : AbstractEntityContainer
    {
        public MockEntityContainer(string typeId) : base(typeId)
        {
        }
    }
}
