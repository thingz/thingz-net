﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Thingz.Common.Tests
{
    public class LinkTests
    {
        [Fact]
        public void CreatedWithoutValidator_ValidateSucceeds()
        {
            var link = new Link<Entity>();
            Assert.True(link.Validate(new Entity()));
        }

        [Fact]
        public void CreatedWithValidator_ValidateCallsDelegate()
        {
            bool delegateCalled = false;
            Func<Entity, bool> action = (i) => delegateCalled = true;
            var link = new Link<Entity>(action);
            link.Validate(new Entity());

            Assert.True(delegateCalled);
        }

        [Fact]
        public void Created_ValidateFailsOnWrongType()
        {
            var link = new Link<Entity>();
            Assert.False(link.Validate(""));
        }
    }
}
