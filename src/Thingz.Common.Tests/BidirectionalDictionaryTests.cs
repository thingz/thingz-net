﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Thingz.Common.Tests
{
    public class BidirectionalDictionaryTests
    {
        [Fact]
        public void DuplicateKey_Throws()
        {
            const string key = "KEY";

            var dictionary = new BidirectionalDictionary<string, object>();
            dictionary.Add(key, new object());
            Assert.Throws<ArgumentException>(() =>
                dictionary.Add(key, new object()));
        }

        [Fact]
        public void DuplicateValue_Throws()
        {
            var value = new object();

            var dictionary = new BidirectionalDictionary<string, object>();
            dictionary.Add("a", value);
            Assert.Throws<ArgumentException>(() =>
                dictionary.Add("b", value));
        }
    }
}
