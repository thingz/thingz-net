﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Thingz.Common.Tests
{
    public class PartTests
    {
        [Fact]
        public void Created_CanSetTarget()
        {
            var part = new Part<Entity>();
            part.Target = new Entity();
        }

        [Fact]
        public void TargetSet_CannotResetTarget()
        {
            var part = new Part<Entity>();
            part.Target = new Entity();
            Assert.Throws<InvalidOperationException>(() =>
                part.Target = new Entity());
        }
    }
}
