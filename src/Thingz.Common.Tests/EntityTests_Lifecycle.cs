﻿using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.EventsArgs;
using Thingz.Common.Internal;
using Thingz.Common.Tests.Mocks;
using Xunit;

namespace Thingz.Common.Tests
{
    public class EntityTests_Lifecycle
    {
        [Theory]
        [MemberData(nameof(Allocated), EntityState.Allocated)]
        [MemberData(nameof(Created), EntityState.Created)]
        [MemberData(nameof(Initialized), EntityState.Initialized)]
        [MemberData(nameof(Configured), EntityState.Configured)]
        [MemberData(nameof(Ready), EntityState.Ready)]
        public void State(Entity instance, EntityState state)
        {
            Assert.Equal(state, instance.State);
        }

        [Theory]
        [MemberData(nameof(Allocated), EntityState.Created)]
        [MemberData(nameof(Created), EntityState.Created)]
        [MemberData(nameof(Initialized), EntityState.Initialized)]
        [MemberData(nameof(Configured), EntityState.Configured)]
        [MemberData(nameof(Ready), EntityState.Ready)]
        public void Create_State(Entity instance, EntityState state)
        {
            instance.Create(GetEmptyCreationArgs());
            Assert.Equal(state, instance.State);
        }

        [Theory]
        [MemberData(nameof(AllocatedMock), "OnCreate")]
        [MemberData(nameof(CreatedMock), "OnCreate")]
        [MemberData(nameof(InitializedMock), "OnCreate")]
        [MemberData(nameof(InitializedMock), "OnInitialize")]
        [MemberData(nameof(InitializedMock), "OnConfigure")]
        [MemberData(nameof(ConfiguredMock), "OnCreate")]
        [MemberData(nameof(ConfiguredMock), "OnInitialize")]
        [MemberData(nameof(ConfiguredMock), "OnConfigure")]
        [MemberData(nameof(ConfiguredMock), "OnStart")]
        [MemberData(nameof(ReadyMock), "OnCreate")]
        [MemberData(nameof(ReadyMock), "OnInitialize")]
        [MemberData(nameof(ReadyMock), "OnConfigure")]
        [MemberData(nameof(ReadyMock), "OnStart")]
        public void Create_CallbackCalledOnce(Mock<Entity> mock, string callbackName)
        {
            mock.Object.Create(GetEmptyCreationArgs());
            mock.Protected().Verify(callbackName, Times.Once());
        }

        [Theory]
        [MemberData(nameof(CreatedMock), "OnConfigure")]
        [MemberData(nameof(CreatedMock), "OnStart")]
        [MemberData(nameof(InitializedMock), "OnStart")]
        public void Create_CallbackNotCalled(Mock<Entity> mock, string callbackName)
        {
            try
            {
                mock.Object.Create(GetEmptyCreationArgs());
            }
            catch
            { }
            mock.Protected().Verify(callbackName, Times.Never());
        }

        [Theory]
        [MemberData(nameof(Allocated))]
        public void Create_RaisesEntityStateChanged(Entity instance)
        {
            Assert.Raises<EntityStateChangedArgs>(
                handler => instance.EntityStateChanged += handler,
                handler => instance.EntityStateChanged -= handler,
                () => instance.Create(GetEmptyCreationArgs()));
        }

        [Theory]
        [MemberData(nameof(Created))]
        [MemberData(nameof(Initialized))]
        [MemberData(nameof(Configured))]
        [MemberData(nameof(Ready))]
        public void Create_DoesNotRaiseEntityStateChanged(Entity instance)
        {
            bool eventRaised = false;
            instance.EntityStateChanged += (sender, e) => eventRaised = true;
            instance.Create(GetEmptyCreationArgs());
            Assert.False(eventRaised);
        }

        [Fact]
        public void Allocated_Initialize_Throws()
        {
            var instance = new Entity();
            Assert.Throws<InvalidOperationException>(() =>
                instance.Initialize());
        }

        [Fact]
        public void Allocated_Configure_Throws()
        {
            var instance = new Entity();
            Assert.Throws<InvalidOperationException>(() =>
                instance.Configure());
        }

        [Fact]
        public void Allocated_Start_Throws()
        {
            var instance = new Entity();
            Assert.Throws<InvalidOperationException>(() =>
                instance.Start());
        }

        [Theory]
        [MemberData(nameof(Allocated))]
        [MemberData(nameof(Configured))]
        [MemberData(nameof(Ready))]
        public void Configure_DoesNotRaiseEntityStateChanged(Entity instance)
        {
            bool eventRaised = false;
            instance.EntityStateChanged += (sender, e) => eventRaised = true;
            try
            {
                instance.Configure();
            }
            catch
            { }
            Assert.False(eventRaised);
        }

        [Theory]
        [MemberData(nameof(Allocated))]
        [MemberData(nameof(Ready))]
        public void Start_DoesNotRaiseEntityStateChanged(Entity instance)
        {
            bool eventRaised = false;
            instance.EntityStateChanged += (sender, e) => eventRaised = true;
            try
            {
                instance.Start();
            }
            catch
            { }
            Assert.False(eventRaised);
        }

        [Fact]
        public void PartNotInitialized_Blocks_Initialize()
        {
            var partMock = GetCreatedMock();
            var instance = new EntityWithPart();
            instance.Part.Target = partMock.Object;

            var args = GetCreationArgsWithPart("Part", instance.Part);
            instance.Create(args);

            Assert.Equal(EntityState.Created, instance.State);
        }

        [Fact]
        public void PartInitialized_Triggers_Initialize()
        {
            var partMock = GetCreatedMock();
            var instance = new EntityWithPart();
            instance.Part.Target = partMock.Object;

            var args = GetCreationArgsWithPart("Part", instance.Part);
            instance.Create(args);

            partMock.Protected().Setup("OnInitialize").CallBase();
            partMock.Object.Initialize();
            Assert.NotEqual(EntityState.Created, instance.State);
        }

        [Fact]
        public void PartDeinitialized_Triggers_Deinitialize()
        {
            var partMock = GetInitializedMock();
            var instance = new EntityWithPart();
            instance.Part.Target = partMock.Object;

            var args = GetCreationArgsWithPart("Part", instance.Part);
            instance.Create(args);

            partMock.Object.Deinitialize();
            Assert.NotEqual(EntityState.Initialized, instance.State);
        }

        [Fact]
        public void LinkNotSet_Blocks_Configure()
        {
            var instance = new EntityWithLink();

            var args = GetCreationArgsWithLink("Link", instance.Link);
            instance.Create(args);
            instance.Initialize();

            Assert.Equal(EntityState.Initialized, instance.State);
        }

        [Fact]
        public void LinkSet_Triggers_Configure()
        {
            var instance = new EntityWithLink();

            var args = GetCreationArgsWithLink("Link", instance.Link);
            instance.Create(args);

            instance.Link.Target = GetReadyMock().Object;
            Assert.NotEqual(EntityState.Initialized, instance.State);
        }

        [Fact]
        public void LinkSet_DoesNotBlock_Start()
        {
            var instance = new EntityWithLink();

            var args = GetCreationArgsWithLink("Link", instance.Link);
            instance.Link.Target = GetReadyMock().Object;
            instance.Create(args);
            instance.Initialize();
            Assert.Equal(EntityState.Ready, instance.State);
        }

        [Fact]
        public void LinkReset_Triggers_Deconfigure()
        {
            var instance = new EntityWithLink();

            var args = GetCreationArgsWithLink("Link", instance.Link);
            instance.Link.Target = GetReadyMock().Object;
            instance.Create(args);
            instance.Initialize();

            instance.Link.Target = null;
            Assert.Equal(EntityState.Initialized, instance.State);
        }

        private (Entity Instance, Mock<Entity> PartMock) GetConfiguredInstance_WithCreatedStartBlockPart()
        {
            var partMock = GetCreatedMock();
            partMock.Protected().Setup("OnInitialize").CallBase();
            partMock.Protected().Setup("OnStart").Throws(new Exception("Deliberate block to fix state to Configured"));

            var instance = new EntityWithPart();
            instance.Part.Target = partMock.Object;
            var args = GetCreationArgsWithPart("Part", instance.Part);
            instance.Create(args);
            instance.Initialize();

            return (instance, partMock);
        }

        [Fact]
        public void PartNotReady_Blocks_Start()
        {
            var instance = GetConfiguredInstance_WithCreatedStartBlockPart().Instance;
            Assert.Equal(EntityState.Configured, instance.State);
        }

        [Fact]
        public void PartReady_Triggers_Start()
        {
            var (instance, partMock) = GetConfiguredInstance_WithCreatedStartBlockPart();

            partMock.Protected().Setup("OnStart").CallBase();
            partMock.Object.Start();

            Assert.Equal(EntityState.Ready, instance.State);
        }

        [Fact]
        public void PartStopping_Triggers_Stop()
        {
            var partMock = GetReadyMock();
            var instance = new EntityWithPart();
            var args = GetCreationArgsWithPart("Part", instance.Part);
            instance.Part.Value = partMock.Object;
            instance.Create(args);
            instance.Initialize();

            partMock.Object.Stop();

            Assert.Equal(EntityState.Configured, instance.State);
        }

        private (Entity Instance, Mock<Entity> LinkMock) GetConfiguredInstance_WithLinkWithStartBlock()
        {
            var instance = new EntityWithLink();
            var linkMock = GetConfiguredMock();

            var args = GetCreationArgsWithLink("Link", instance.Link);
            instance.Link.Target = linkMock.Object;
            instance.Create(args);
            instance.Initialize();
            return (instance, linkMock);
        }

        [Fact]
        public void LinkNotReady_Blocks_Start()
        {
            var instance = GetConfiguredInstance_WithLinkWithStartBlock().Instance;
            Assert.Equal(EntityState.Configured, instance.State);
        }

        [Fact]
        public void LinkReady_Triggers_Start()
        {
            var (instance, linkMock) = GetConfiguredInstance_WithLinkWithStartBlock();

            linkMock.Protected().Setup("OnStart").CallBase();
            linkMock.Object.Start();

            Assert.Equal(EntityState.Ready, instance.State);
        }

        [Fact]
        public void LinkStopping_Triggers_Stop()
        {
            var linkMock = GetReadyMock();
            var instance = new EntityWithLink();
            var args = GetCreationArgsWithLink("Link", instance.Link);
            instance.Link.Target = linkMock.Object;
            instance.Create(args);
            instance.Initialize();

            linkMock.Object.Stop();

            Assert.Equal(EntityState.Configured, instance.State);
        }


        public static IEnumerable<object[]> Allocated() => new[] { new object[] { new Entity() } };
        public static IEnumerable<object[]> Allocated(EntityState state) => new[] { new object[] { new Entity(), state } };
        public static IEnumerable<object[]> AllocatedMock() => new[] { new[] { new Mock<Entity>() } };
        public static IEnumerable<object[]> AllocatedMock(string name) => new[] { new object[] { new Mock<Entity>(), name } };
        public static IEnumerable<object[]> Created() => new[] { new[] { GetCreatedMock().Object } };
        public static IEnumerable<object[]> Created(EntityState state) => new[] { new object[] { GetCreatedMock().Object, state } };
        public static IEnumerable<object[]> CreatedMock() => new[] { new[] { GetCreatedMock() } };
        public static IEnumerable<object[]> CreatedMock(string name) => new[] { new object[] { GetCreatedMock(), name } };
        public static IEnumerable<object[]> Initialized() => new[] { new[] { GetInitializedMock().Object } };
        public static IEnumerable<object[]> Initialized(EntityState state) => new[] { new object[] { GetInitializedMock().Object, state } };
        public static IEnumerable<object[]> InitializedMock() => new[] { new[] { GetInitializedMock() } };
        public static IEnumerable<object[]> InitializedMock(string name) => new[] { new object[] { GetInitializedMock(), name } };
        public static IEnumerable<object[]> Configured() => new[] { new[] { GetConfiguredMock().Object } };
        public static IEnumerable<object[]> Configured(EntityState state) => new[] { new object[] { GetConfiguredMock().Object, state } };
        public static IEnumerable<object[]> ConfiguredMock() => new[] { new[] { GetConfiguredMock() } };
        public static IEnumerable<object[]> ConfiguredMock(string name) => new[] { new object[] { GetConfiguredMock(), name } };
        public static IEnumerable<object[]> Ready() => new[] { new[] { GetReadyMock().Object } };
        public static IEnumerable<object[]> Ready(EntityState state) => new[] { new object[] { GetReadyMock().Object, state } };
        public static IEnumerable<object[]> ReadyMock() => new[] { new[] { GetReadyMock() } };
        public static IEnumerable<object[]> ReadyMock(string name) => new[] { new object[] { GetReadyMock(), name } };


        public static Mock<Entity> GetReadyMock()
        {
            var mock = GetCreatedMock();
            mock.Object.Initialize();
            return mock;
        }

        public static Mock<Entity> GetConfiguredMock()
        {
            var mock = GetCreatedMock();
            mock.Protected().Setup("OnStart").Throws(new Exception("Deliberate block to fix state to Configured"));
            mock.Object.Initialize();
            return mock;
        }

        public static Mock<Entity> GetInitializedMock()
        {
            var mock = GetCreatedMock();
            mock.Protected().Setup("OnConfigure").Throws(new Exception("Deliberate block to fix state to Initialized"));
            mock.Object.Initialize();
            return mock;
        }

        public static Mock<Entity> GetCreatedMock()
        {
            var mock = new Mock<Entity>();
            mock.Object.Create(GetEmptyCreationArgs());
            return mock;
        }

        private static EntityCreationArgs GetEmptyCreationArgs()
        {
            return new EntityCreationArgs(
                            new StaticPartCollection(),
                            new Dictionary<string, PartCollection>(),
                            new StaticLinkCollection(),
                            new Dictionary<string, LinkCollection>());
        }

        private static EntityCreationArgs GetCreationArgsWithPart(string partName, Part part)
        {
            return new EntityCreationArgs(
                            new StaticPartCollection(new Dictionary<string, Part>() { { partName, part } }),
                            new Dictionary<string, PartCollection>(),
                            new StaticLinkCollection(),
                            new Dictionary<string, LinkCollection>());
        }

        private static EntityCreationArgs GetCreationArgsWithLink(string linkName, Link link)
        {
            return new EntityCreationArgs(
                            new StaticPartCollection(),
                            new Dictionary<string, PartCollection>(),
                            new StaticLinkCollection(new Dictionary<string, Link>() { { linkName, link } }),
                            new Dictionary<string, LinkCollection>());
        }
    }
}
