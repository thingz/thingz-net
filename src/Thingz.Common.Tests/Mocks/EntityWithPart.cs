﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Common.Tests.Mocks
{
    public class EntityWithPart : Entity
    {
        public Part<Entity> Part { get; } = new Part<Entity>();
    }
}
