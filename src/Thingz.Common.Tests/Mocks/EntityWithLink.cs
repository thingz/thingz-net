﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Common.Tests.Mocks
{
    public class EntityWithLink : Entity
    {
        public Link<Entity> Link { get; } = new Link<Entity>();
    }
}
