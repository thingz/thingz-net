﻿using System;
using System.Linq;
using Thingz.Common;
using Thingz.Entities.Basic;
using Thingz.Hosting.Instances;
using Thingz.Hosting.Types;

namespace Thingz.Host
{
    class Program
    {
        static Setup<Root> setup;

        static void Main(string[] args)
        {
            Console.WriteLine("Generating Repository...");
            var repo = Repository.Create<Entity>();

            Console.WriteLine("Loading Entity Types...");
            repo.LoadAssembly<Root>();

            Console.WriteLine("Generating Setup...");
            setup = new Setup<Root>(repo);
            setup.Logged += Setup_Logged;
            setup.EntityStateChanged += Setup_EntityStateChanged;

            Console.WriteLine("Starting...");
            setup.Start();

            foreach (var instance in setup)
                Console.WriteLine(setup[instance] + ": " + instance.State.ToString());

            Console.WriteLine("Ready. Press any key to quit.");
            Console.ReadKey();

            Console.WriteLine("Destroying...");
            setup.Root.Destroy();
        }

        private static void Setup_Logged(object sender, string e)
        {
            Console.WriteLine("\t[" + sender.ToString() + "] " + e);
        }

        private static void Setup_EntityStateChanged(object sender, Common.EventsArgs.EntityStateChangedArgs e)
        {
            string senderId = sender.ToString();
            if (sender is Entity instance && setup.Contains(instance))
                senderId += ": " + setup[instance];

            Console.WriteLine("\t[" + senderId + "] EntityStateChanged: " +
                Enum.GetName(e.OldState.GetType(), e.OldState) + " => " +
                Enum.GetName(e.NewState.GetType(), e.NewState));
        }
    }
}
