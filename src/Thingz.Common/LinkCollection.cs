﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.EventsArgs;
using Thingz.Common.Internal;

namespace Thingz.Common
{
    public class LinkCollection : StaticLinkCollection
    {
        public LinkCollection()
        { }

        public LinkCollection(IEnumerable<KeyValuePair<string, Link>> links) : base(links)
        { }
    }

    public class LinkCollection<TEntity> : LinkCollection where TEntity : Entity
    {
        private readonly object _lock = new object();

        public Link<TEntity> CreateLink(string name)
        {
            lock (_lock)
            {
                var link = new Link<TEntity>();
                Add(name, link);
                return link;
            }
        }

        public void RemoveLink(string name)
        {
            lock (_lock)
            {
                var link = this[name];
                link.Value = null;
                Remove(name);
            }
        }
    }
}
