﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.Internal;

namespace Thingz.Common
{
    public abstract class Link : AbstractEntityContainer
    {
        public abstract bool Validate(object target);

        protected Link(string typeId) : base(typeId)
        { }
    }

    public class Link<T> : Link where T : Entity
    {
        private readonly Func<T, bool> _validator;

        public T Target
        {
            get => Value as T;
            set => Value = value;
        }

        public Link() : this(i => true)
        { }

        public Link(Func<T, bool> validator) : base(typeof(T).FullName)
        {
            _validator = validator;
        }

        public override bool Validate(object target)
        {
            if (target is T typedTarget)
                return _validator.Invoke(typedTarget);
            return false;
        }
    }
}
