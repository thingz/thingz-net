﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.EventsArgs;

namespace Thingz.Common.Internal
{
    public class StaticPartCollection : AbstractEntityContainerCollection<Part>
    {
        public event EventHandler<EntityRequestedArgs> InLinkRequested;

        public StaticPartCollection()
        { }

        public StaticPartCollection(IEnumerable<KeyValuePair<string, Part>> parts) : base(parts)
        {
        }

        public void Create()
        {
            foreach(var part in this)
            {
                if(part.Value.Value == null)
                {
                    var args = new EntityRequestedArgs(part.Key, part.Value.TypeId);
                    part.Value.Value = CreatePart(FireItemRequestedEvent, args);
                }
            }
        }

        public T CreateStaticPart<T>(Part partToResolve) where T : Entity
        {
            if (!Contains(partToResolve))
                throw new ArgumentException("Part doesn't exist");
            if (partToResolve.Value != null)
                throw new InvalidOperationException("Part already set");

            var args = new EntityRequestedArgs(this[partToResolve], typeof(T).FullName);
            partToResolve.Value = CreatePart(FireItemRequestedEvent, args);
            return partToResolve.Value as T;
        }

        protected Entity CreatePart(Action<EntityRequestedArgs> eventToFire, EntityRequestedArgs args)
        {
            eventToFire(args);
            var part = args.ResolvedEntity;
            if (part == null)
                throw new Exception("Requested part was not resolved");
            return part;
        }

        protected void FireInLinkRequestedEvent(EntityRequestedArgs args)
        {
            InLinkRequested?.Invoke(this, args);
        }
    }
}
