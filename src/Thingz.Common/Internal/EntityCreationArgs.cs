﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Common.Internal
{
    public class EntityCreationArgs
    {
        public StaticPartCollection StaticParts { get; }
        public IDictionary<string, PartCollection> DynamicParts { get; }
        public StaticLinkCollection StaticLinks { get; }
        public IDictionary<string, LinkCollection> DynamicLinks { get; }

        public EntityCreationArgs(StaticPartCollection staticParts, IDictionary<string, PartCollection> dynamicParts, StaticLinkCollection staticLinks, IDictionary<string, LinkCollection> dynamicLinks)
        {
            StaticParts = staticParts;
            DynamicParts = dynamicParts;
            StaticLinks = staticLinks;
            DynamicLinks = dynamicLinks;
        }
    }
}
