﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.EventsArgs;

namespace Thingz.Common.Internal
{
    public class StaticLinkCollection : AbstractEntityContainerCollection<Link>
    {
        public event EventHandler<TargetChangedArgs> LinkChanged;

        public StaticLinkCollection()
        { }

        public StaticLinkCollection(IEnumerable<KeyValuePair<string, Link>> links) : base(links)
        { }

        protected override void Add(string key, Link value)
        {
            base.Add(key, value);
            value.TargetChanged += Item_TargetChanged;
        }

        private void Item_TargetChanged(object sender, TargetChangedArgs e)
        {
            if (sender is Link link)
            {
                var args = new TargetChangedArgs(this[link], e.OldTarget, e.NewTarget);
                LinkChanged?.Invoke(this, args);
            }
        }

        public void ResetTargets()
        {
            foreach (var item in this)
                item.Value.Value = null;
        }
    }
}
