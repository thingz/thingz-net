﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.EventsArgs;

namespace Thingz.Common.Internal
{
    public abstract class AbstractEntityContainerCollection<TContainer> : IEnumerable<KeyValuePair<string, TContainer>> where TContainer : AbstractEntityContainer
    {
        private readonly BidirectionalDictionary<string, TContainer> _items = new BidirectionalDictionary<string, TContainer>();

        public event EventHandler<EntityRequestedArgs> ItemRequested;
        public event EventHandler<EntityStateChangedArgs> ItemStateChanged;

        protected AbstractEntityContainerCollection()
        { }

        protected AbstractEntityContainerCollection(IEnumerable<KeyValuePair<string, TContainer>> items)
        {
            foreach (var item in items)
                Add(item.Key, item.Value);
        }

        public TContainer this[string key] => _items[key];
        public string this[TContainer value] => _items[value];

        protected virtual void Add(string key, TContainer value)
        {
            _items.Add(key, value);
            value.TargetStateChanged += Item_TargetStateChanged;
        }

        private void Item_TargetStateChanged(object sender, EntityStateChangedArgs e)
        {
            ItemStateChanged?.Invoke(sender, e);
        }

        protected virtual void Remove(string key)
        {
            var item = this[key];
            item.TargetStateChanged -= Item_TargetStateChanged;
            _items.Remove(key);
        }
        protected void FireItemRequestedEvent(EntityRequestedArgs e) => ItemRequested?.Invoke(this, e);

        public bool Contains(TContainer value) => _items.Contains(value);
        public bool ContainsKey(string key) => _items.ContainsKey(key);
        public IEnumerator<KeyValuePair<string, TContainer>> GetEnumerator() => _items.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _items.GetEnumerator();

        public void Initialize()
        {
            foreach (var item in _items.Values)
                item.Value.Initialize();
        }

        public void Configure()
        {
            foreach (var item in _items.Values)
                item.Value.Configure();
        }

        public void Start()
        {
            foreach (var item in _items.Values)
                item.Value.Start();
        }

        public void Stop()
        {
            foreach (var item in _items.Values)
                item.Value.Stop();
        }

        public void Deconfigure()
        {
            foreach (var item in _items.Values)
                item.Value.Deconfigure();
        }

        public void Deinitialize()
        {
            foreach (var item in _items.Values)
                item.Value.Deinitialize();
        }

        public void Destroy()
        {
            foreach (var item in _items.Values)
                item.Value.Destroy();
        }
    }
}
