﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.EventsArgs;

namespace Thingz.Common.Internal
{
    public abstract class AbstractEntityContainer
    {
        public event EventHandler<TargetChangedArgs> TargetChanged;
        public event EventHandler<EntityStateChangedArgs> TargetStateChanged;

        public string TypeId { get; }

        private readonly object _valueLock = new object();
        private Entity _value = null;
        public virtual Entity Value
        {
            get => _value;
            set
            {
                lock(_valueLock)
                {
                    var oldValue = _value;
                    if (oldValue != null)
                        oldValue.EntityStateChanged -= Target_EntityStateChanged;
                    _value = value;
                    TargetChanged?.Invoke(this, new TargetChangedArgs("", oldValue, _value));
                    if(value != null)
                        _value.EntityStateChanged += Target_EntityStateChanged;

                }
            }
        }

        private void Target_EntityStateChanged(object sender, EntityStateChangedArgs e)
        {
            TargetStateChanged?.Invoke(sender, e);
        }

        protected AbstractEntityContainer(string typeId)
        {
            TypeId = typeId;
        }
    }
}
