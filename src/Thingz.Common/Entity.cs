﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Common.EventsArgs;
using Thingz.Common.Internal;

namespace Thingz.Common
{
    public enum EntityState
    {
        Allocated,
        Created,
        Deinitializing,
        Initialized,
        Configuring,
        Configured,
        Stopping,
        Ready
    }

    public class Entity
    {
        protected StaticPartCollection StaticParts { get; private set; }
        private readonly Dictionary<string, PartCollection> _dynamicParts = new Dictionary<string, PartCollection>();

        protected IEnumerable<KeyValuePair<string, Part>> Parts => StaticParts.Concat(_dynamicParts.SelectMany(i => i.Value));
        protected IEnumerable<KeyValuePair<string, Link>> Links => StaticLinks.Concat(_dynamicLinks.SelectMany(i => i.Value));

        protected StaticLinkCollection StaticLinks { get; private set; }
        private readonly Dictionary<string, LinkCollection> _dynamicLinks = new Dictionary<string, LinkCollection>();

        public event EventHandler<EntityStateChangedArgs> EntityStateChanged;
        public event EventHandler<TargetChangedArgs> LinkChanged;
        public event EventHandler<string> Logged;

        public event EventHandler<EntityRequestedArgs> PartRequested;
        public event EventHandler<EntityRequestedArgs> LinkRequested;
        public event EventHandler<EntityRequestedArgs> InLinkRequested;

        private bool isInitializing = false;
        private bool isStarting = false;

        private readonly object _stateLock = new object();
        private EntityState _state = EntityState.Allocated;
        public EntityState State
        {
            get => _state;
            private set
            {
                lock (_stateLock)
                {
                    var oldState = _state;
                    _state = value;
                    // Trigger event unless recovering from failed Configure()
                    if(oldState != EntityState.Configuring || _state != EntityState.Initialized)
                        EntityStateChanged?.Invoke(this, new EntityStateChangedArgs(oldState, _state));
                }
            }
        }

        public void Create(EntityCreationArgs args)
        {
            lock(_stateLock)
            {
                if (State != EntityState.Allocated)
                    return;

                SetPartIndex(args.StaticParts, args.DynamicParts);
                SetLinkIndex(args.StaticLinks, args.DynamicLinks);

                OnCreate();
                State = EntityState.Created;
                CreateParts();
            }
        }

        private void SetPartIndex(StaticPartCollection staticParts, IDictionary<string, PartCollection> dynamicParts)
        {
            StaticParts = staticParts;
            StaticParts.ItemRequested += PartCollection_ItemRequested;
            StaticParts.ItemStateChanged += Part_EntityStateChanged;

            foreach (var partCollection in dynamicParts)
            {
                _dynamicParts.Add(partCollection.Key, partCollection.Value);
                partCollection.Value.ItemRequested += PartCollection_ItemRequested;
                partCollection.Value.InLinkRequested += Part_InLinkRequested;
                partCollection.Value.ItemStateChanged += Part_EntityStateChanged;
            }
        }

        private void SetLinkIndex(StaticLinkCollection staticLinks, IDictionary<string, LinkCollection> dynamicLinks)
        {
            StaticLinks = staticLinks;
            StaticLinks.ItemRequested += LinkCollection_ItemRequested;
            StaticLinks.ItemStateChanged += Link_EntityStateChanged;
            StaticLinks.LinkChanged += LinkCollection_LinkChanged;

            foreach(var linkCollection in dynamicLinks)
            {
                dynamicLinks.Add(linkCollection.Key, linkCollection.Value);
                linkCollection.Value.ItemRequested += LinkCollection_ItemRequested;
                linkCollection.Value.ItemStateChanged += Link_EntityStateChanged;
                linkCollection.Value.LinkChanged += LinkCollection_LinkChanged;
            }
        }

        private void CreateParts()
        {
            StaticParts.Create();
            foreach (var partCollection in _dynamicParts.Values)
                partCollection.Create();
        }

        private void TryInitialize()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                Log("Could not initialize: " + ex.GetType().Name + ": " + ex.Message);
            }
        }

        public void Initialize()
        {
            lock (_stateLock)
            {
                if (State == EntityState.Ready || State == EntityState.Configured)
                    return;
                if (isInitializing)
                    return;
                isInitializing = true;
                try
                {
                    if (State != EntityState.Initialized)
                    {
                        if (State == EntityState.Allocated)
                            throw new InvalidOperationException("Create must be called before Initialize!");
                        if (State != EntityState.Created)
                            throw new InvalidOperationException("Initialize() requires state Created");

                        InitializeParts();
                        OnInitialize();
                        InitializeParts();
                        State = EntityState.Initialized;
                    }
                    TryConfigure();
                }
                finally
                {
                    isInitializing = false;
                }
            }
        }

        private void InitializeParts()
        {
            foreach (var part in Parts)
                part.Value.Value.Initialize();
        }

        private void TryConfigure()
        {
            try
            {
                Configure();
            }
            catch (Exception ex)
            {
                Log("Could not configure: " + ex.GetType().Name + ": " + ex.Message);
            }
        }

        public void Configure()
        {
            lock (_stateLock)
            {
                ConfigureThis();
                ConfigureParts();

                TryStart();
            }
        }

        public void ConfigureThis()
        {
            lock(_stateLock)
            {
                if (State == EntityState.Configuring || State == EntityState.Configured || State == EntityState.Ready)
                    return;

                if (State != EntityState.Initialized)
                    throw new InvalidOperationException("Configure() requires state Initialized");

                foreach (var link in Links)
                {
                    if(link.Value.Value == null)
                        throw new InvalidOperationException("Configure() requires all Links to be set");
                }

                try
                {
                    State = EntityState.Configuring;
                    OnConfigure();
                }
                catch
                {
                    State = EntityState.Initialized;
                    throw;
                }
                State = EntityState.Configured;
            }
        }

        private void ConfigureParts()
        {
            foreach (var part in Parts)
                part.Value.Value.Configure();
        }

        private void TryStart()
        {
            try
            {
                Start();
            }
            catch (Exception ex)
            {
                Log("Could not configure: " + ex.GetType().Name + ": " + ex.Message);
            }
        }

        public void Start()
        {
            lock (_stateLock)
            {
                if (State == EntityState.Ready)
                    return;

                if (State != EntityState.Configured)
                    throw new InvalidOperationException("Start() requires state Configured");

                if (isStarting)
                    return;
                isStarting = true;
                try
                {
                    foreach (var link in Links)
                    {
                        if (link.Value.Value.State != EntityState.Ready)
                            throw new InvalidOperationException("Configure() requires all Links to be set");
                    }

                    StartParts();
                    OnStart();
                    StartParts();
                    State = EntityState.Ready;
                }
                finally
                {
                    isStarting = false;
                }
            }
        }

        private void StartParts()
        {
            StaticParts.Start();
            foreach (var partCollection in _dynamicParts.Values)
                partCollection.Start();
        }

        public void Stop()
        {
            lock(_stateLock)
            {
                StopThis();
                StopParts();
            }
        }

        public void StopThis()
        {
            lock (_stateLock)
            {
                if (State != EntityState.Ready)
                    return;

                State = EntityState.Stopping;
                OnStop();
                State = EntityState.Configured;
            }
        }

        private void StopParts()
        {
            if (State != EntityState.Configured)
                return;

            StaticParts.Stop();
            foreach (var partCollection in _dynamicParts.Values)
                partCollection.Stop();
        }

        public void Deconfigure()
        {
            lock(_stateLock)
            {
                Stop();
                if (State != EntityState.Configured)
                    return;

                DeconfigureParts();
                OnDeconfigure();
                State = EntityState.Initialized;
            }
        }

        private void DeconfigureParts()
        {
            StaticParts.Deconfigure();
            foreach (var partCollection in _dynamicParts.Values)
                partCollection.Deconfigure();
        }

        public void Deinitialize()
        {
            lock (_stateLock)
            {
                DeinitializeThis();
                DeinitializeParts();
            }
        }

        public void DeinitializeThis()
        {
            lock(_stateLock)
            {
                Deconfigure();
                if (State != EntityState.Initialized)
                    return;
                State = EntityState.Deinitializing;
                OnDeinitialize();
                State = EntityState.Created;
            }
        }

        private void DeinitializeParts()
        {
            StaticParts.Deinitialize();
            foreach (var partCollection in _dynamicParts.Values)
                partCollection.Deinitialize();
        }

        public void Destroy()
        {
            lock(_stateLock)
            {
                Deinitialize();
                if (State != EntityState.Created)
                    return;

                StaticParts.Destroy();
                foreach (var partCollection in _dynamicParts.Values)
                    partCollection.Destroy();

                StaticLinks.ResetTargets();
                foreach (var linkCollection in _dynamicLinks.Values)
                    linkCollection.ResetTargets();

                OnDestroy();
                State = EntityState.Allocated;
            }
        }

        protected virtual void OnCreate() { }
        protected virtual void OnInitialize() { }
        protected virtual void OnConfigure() { }
        protected virtual void OnStart() { }
        protected virtual void OnStop() { }
        protected virtual void OnDeconfigure() { }
        protected virtual void OnDeinitialize() { }
        protected virtual void OnDestroy() { }

        protected void Log(string message) => Logged?.Invoke(this, message);

        private void LinkCollection_ItemRequested(object sender, EntityRequestedArgs e) => LinkRequested?.Invoke(this, e);

        private void LinkCollection_LinkChanged(object sender, TargetChangedArgs e)
        {
            LinkChanged?.Invoke(this, e);
            if (e.NewTarget != null && State == EntityState.Initialized)
                TryConfigure();
            else if (e.NewTarget == null)
            {
                if (State == EntityState.Ready)
                    StopThis();
                if (State == EntityState.Configured)
                    Deconfigure();
            }
        }

        private void PartCollection_ItemRequested(object sender, EntityRequestedArgs e)
        {
            PartRequested?.Invoke(this, e);
        }

        private void Part_InLinkRequested(object sender, EntityRequestedArgs e)
        {
            InLinkRequested?.Invoke(this, e);
        }

        private void Part_EntityStateChanged(object sender, EntityStateChangedArgs e)
        {
            switch (e.NewState)
            {
                case EntityState.Initialized:
                    if(State == EntityState.Created)
                        TryInitialize();
                    break;

                case EntityState.Configuring:
                    ConfigureThis();
                    break;

                case EntityState.Ready:
                    if (State == EntityState.Configured)
                        TryStart();
                    break;

                case EntityState.Stopping:
                    StopThis();
                    break;

                case EntityState.Deinitializing:
                    DeinitializeThis();
                    break;
            }
        }

        private void Link_EntityStateChanged(object sender, EntityStateChangedArgs e)
        {
            switch(e.NewState)
            {
                case EntityState.Ready:
                    if (State == EntityState.Configured)
                        TryStart();
                    break;

                case EntityState.Stopping:
                    StopThis();
                    break;
            }
        }
    }
}
