﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Common.EventsArgs
{
    public class EntityRequestedArgs : EventArgs
    {
        public string Name { get; }
        public string TypeId { get; }
        public Entity ResolvedEntity { get; set; } = null;

        public EntityRequestedArgs(string name, string typeId)
        {
            Name = name;
            TypeId = typeId;
        }
    }
}
