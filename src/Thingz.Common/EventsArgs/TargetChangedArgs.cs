﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Common.EventsArgs
{
    public class TargetChangedArgs : EventArgs
    {
        public string Name { get; }
        public Entity OldTarget { get; }
        public Entity NewTarget { get; }

        public TargetChangedArgs(string name, Entity oldTarget, Entity newTarget)
        {
            Name = name;
            OldTarget = oldTarget;
            NewTarget = newTarget;
        }
    }
}
