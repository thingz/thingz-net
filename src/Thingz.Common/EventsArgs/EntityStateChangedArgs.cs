﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Common.EventsArgs
{
    public class EntityStateChangedArgs : EventArgs
    {
        public EntityState OldState { get; }
        public EntityState NewState { get; }

        public EntityStateChangedArgs(EntityState oldState, EntityState newState)
        {
            OldState = oldState;
            NewState = newState;
        }
    }
}
