﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.EventsArgs;
using Thingz.Common.Internal;

namespace Thingz.Common
{
    public abstract class PartCollection : StaticPartCollection
    {
        public abstract void CreateAndInitializeDynamic(string name, string typeId);
    }

    public class PartCollection<TEntity> : PartCollection where TEntity : Entity
    {
        public T CreateAndInitialize<T>(string name) where T : TEntity
        {
            return CreateAndInitialize(name, typeof(T).FullName) as T;
        }

        public TEntity CreateAndInitialize(string name, string typeId)
        {
            var args = new EntityRequestedArgs(name, typeId);
            return CreateAndInitialize(FireItemRequestedEvent, args);
        }

        public TEntity CreateAndInitializeForLink(string name, Entity linkTarget)
        {
            var args = new EntityRequestedArgs(name, typeof(TEntity).FullName);
            args.ResolvedEntity = linkTarget;
            return CreateAndInitialize(FireInLinkRequestedEvent, args);
        }

        private TEntity CreateAndInitialize(Action<EntityRequestedArgs> eventToFire, EntityRequestedArgs args)
        {
            var part = CreateEmptyPart(args.Name);
            try
            {
                part.Target = CreatePart(eventToFire, args) as TEntity;
                part.Target.Initialize();
            }
            catch
            {
                Remove(args.Name);
                throw;
            }
            return part.Target;
        }

        private Part<TEntity> CreateEmptyPart(string name)
        {
            var part = new Part<TEntity>();
            Add(name, part);
            return part;
        }

        public override void CreateAndInitializeDynamic(string name, string typeId) => CreateAndInitialize(name, typeId);
    }
}
