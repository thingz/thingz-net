﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Common
{
    public class BidirectionalDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> _items = new Dictionary<TKey, TValue>();
        private readonly Dictionary<TValue, TKey> _keysByValue = new Dictionary<TValue, TKey>();

        public TValue this[TKey key]
        {
            get => _items[key];
            set => Set(key, value);
        }

        public TKey this[TValue key]
        {
            get => _keysByValue[key];
            set => Set(value, key);
        }

        private void Set(TKey key, TValue value)
        {
            lock (_items)
            {
                _items[key] = value;
                _keysByValue[value] = key;
            }
        }

        public bool TryGetValue(TKey key, out TValue value) => _items.TryGetValue(key, out value);

        public void Add(KeyValuePair<TKey, TValue> item) => Add(item.Key, item.Value);

        public void Add(TKey key, TValue value)
        {
            lock (_items)
            {
                _items.Add(key, value);
                try
                {
                    _keysByValue.Add(value, key);
                }
                catch
                {
                    _items.Remove(key);
                    throw;
                }
            }
        }

        public bool Remove(TKey key)
        {
            lock (_items)
            {
                if(_items.ContainsKey(key))
                    return Remove(key, _items[key]);
            }
            return false;
        }

        public bool Remove(TValue value)
        {
            lock (_items)
            {
                if(_keysByValue.ContainsKey(value))
                    return Remove(_keysByValue[value], value);
            }
            return false;
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            lock (_items)
                return Remove(item.Key, item.Value);
        }

        private bool Remove(TKey key, TValue value)
        {
            lock (_items)
            {
                if (!_items.Remove(key))
                    return false;
                if (!_keysByValue.Remove(value))
                {
                    _items.Add(key, value);
                    return false;
                }
            }
            return true;
        }

        public void Clear()
        {
            lock (_items)
            {
                _items.Clear();
                _keysByValue.Clear();
            }
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            lock (_items)
                return ((IDictionary<TKey, TValue>)_items).Contains(item);
        }

        public bool Contains(TValue value)
        {
            lock (_items)
                return _keysByValue.ContainsKey(value);
        }

        public bool ContainsKey(TKey key)
        {
            lock (_items)
                return _items.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            lock (_items)
                ((IDictionary<TKey, TValue>)_items).CopyTo(array, arrayIndex);
        }

        public ICollection<TKey> Keys => _items.Keys;
        public ICollection<TValue> Values => _items.Values;
        public int Count => _items.Count;
        public bool IsReadOnly => ((IDictionary<TKey, TValue>)_items).IsReadOnly;
        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => ((IReadOnlyDictionary<TKey, TValue>)_items).Keys;
        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => ((IReadOnlyDictionary<TKey, TValue>)_items).Values;
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => _items.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => ((IDictionary<TKey, TValue>)_items).GetEnumerator();
    }
}
