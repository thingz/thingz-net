﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common.Internal;

namespace Thingz.Common
{
    public abstract class Part : AbstractEntityContainer
    {
        private readonly object _valueLock = new object();
        public override Entity Value
        {
            get => base.Value;
            set
            {
                lock (_valueLock)
                {
                    if (Value != null)
                        throw new InvalidOperationException("Value is already set.");
                    base.Value = value;
                }
            }
        }

        protected Part(string typeId) : base(typeId)
        { }
    }

    public class Part<T> : Part where T : Entity
    {
        public T Target
        {
            get => Value as T;
            set => Value = value;
        }

        public Part() : base(typeof(T).FullName)
        { }
    }
}