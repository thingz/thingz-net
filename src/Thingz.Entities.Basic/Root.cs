﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thingz.Common;

namespace Thingz.Entities.Basic
{
    public class Root : Entity
    {
        public Part<Entity> StaticPart { get; } = new Part<Entity>();

        public PartCollection<Entity> Parts { get; } = new PartCollection<Entity>();

        protected override void OnConfigure()
        {
            base.OnConfigure();
            Log("Adding part \"Dynamic Part\"...");
            Parts.CreateAndInitialize<Entity>("DynamicPart");
            Log("Part added.");
        }

        protected override void OnStart()
        {
            base.OnStart();
            Parts.CreateAndInitialize<Entity>("DynamicPart2");
        }
    }
}
