﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Hosting.Types;
using Xunit;

namespace Thingz.Hosting.Tests.Types
{
    public class EntityTypeTests
    {
        [Fact]
        public void Created_IsBaseOf_Positive()
        {
            var et = CreateSimpleEntityType(typeof(object));

            Assert.True(et.IsBaseOf(GetType()));
        }

        [Fact]
        public void Created_IsBaseOf_Negative()
        {
            var et = CreateSimpleEntityType(GetType());

            Assert.False(et.IsBaseOf(typeof(object)));
        }

        [Fact]
        public void Created_Compare_Positive()
        {
            var rawType = GetType();
            var et = CreateSimpleEntityType(rawType);

            Assert.True(et.Compare(rawType));
        }

        [Fact]
        public void Created_Compare_Negative()
        {
            var rawType = GetType();
            var et = CreateSimpleEntityType(rawType);

            Assert.False(et.Compare(typeof(string)));
        }

        private EntityType CreateSimpleEntityType(Type type)
        {
            return new EntityType(type, type.FullName, false,
                new Dictionary<string, string>(),
                new Dictionary<string, string>(),
                new Dictionary<string, string>(),
                new Dictionary<string, string>());
        }
    }
}
