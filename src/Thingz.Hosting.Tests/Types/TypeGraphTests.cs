﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Hosting.Types;
using Xunit;

namespace Thingz.Hosting.Tests.Types
{
    public class TypeGraphTests
    {
        [Fact]
        public void Created_Add_TypeAdded()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var typeToAdd = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(typeToAdd, baseType);

            Assert.Contains(typeToAdd, graph);
        }

        [Fact]
        public void Created_Add_RaisesEvent()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var typeToAdd = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            bool eventRaised = false;
            graph.NodeAdded += (sender, e) => eventRaised = true;
            graph.Add(typeToAdd, baseType);

            Assert.True(eventRaised);
        }

        [Fact]
        public void NonEmpty_Remove_TypeRemoved()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);
            graph.Remove(type.Id);

            Assert.DoesNotContain(type, graph);
        }

        [Fact]
        public void NonEmpty_AddPart_PartAdded()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);
            graph.AddPart("name", type.Id, baseType);

            Assert.NotEmpty(graph.GetParts(baseType.Id).Where(i => i.Value == type));
        }

        [Fact]
        public void NonEmpty_AddPartNotInGraph_Throws()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);

            Assert.Throws<ArgumentException>(() =>
                graph.AddPart("name", type.Id, baseType));
            Assert.Throws<ArgumentException>(() =>
                graph.AddPart("name", type, baseType));
        }

        [Fact]
        public void NonEmpty_AddPartTwice_Throws()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);
            graph.AddPart("name", type.Id, baseType);

            Assert.Throws<ArgumentException>(() =>
                graph.AddPart("name", type.Id, baseType));
            Assert.Throws<ArgumentException>(() =>
                graph.AddPart("name", type, baseType));
        }

        [Fact]
        public void NonEmpty_RemovePart_PartRemoved()
        {
            var partName = "name";
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);
            graph.AddPart(partName, type.Id, baseType);
            graph.RemovePart(partName, baseType);

            Assert.Empty(graph.GetParts(baseType.Id).Where(i => i.Value == type));
        }

        [Fact]
        public void NonEmpty_RemovePartNotInGraph_Throws()
        {
            var partName = "name";
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);

            Assert.Throws<ArgumentException>(() =>
                graph.RemovePart(partName, baseType));
        }

        [Fact]
        public void NonEmpty_AddLink_LinkAdded()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);
            graph.AddLink("name", type.Id, baseType);

            Assert.NotEmpty(graph.GetLinks(baseType.Id).Where(i => i.Value == type));
        }

        [Fact]
        public void NonEmpty_AddLinkNotInGraph_Throws()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);

            Assert.Throws<ArgumentException>(() =>
                graph.AddLink("name", type.Id, baseType));
            Assert.Throws<ArgumentException>(() =>
                graph.AddLink("name", type, baseType));
        }

        [Fact]
        public void NonEmpty_AddLinkTwice_Throws()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);
            graph.AddLink("name", type.Id, baseType);

            Assert.Throws<ArgumentException>(() =>
                graph.AddLink("name", type.Id, baseType));
            Assert.Throws<ArgumentException>(() =>
                graph.AddLink("name", type, baseType));
        }

        [Fact]
        public void NonEmpty_RemoveLink_PartRemoved()
        {
            var linkName = "name";
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);
            graph.AddLink(linkName, type.Id, baseType);
            graph.RemoveLink(linkName, baseType);

            Assert.Empty(graph.GetLinks(baseType.Id).Where(i => i.Value == type));
        }

        [Fact]
        public void NonEmpty_RemoveLinkNotInGraph_Throws()
        {
            var linkName = "name";
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);

            Assert.Throws<ArgumentException>(() =>
                graph.RemoveLink(linkName, baseType));
        }

        [Fact]
        public void Created_GetPartsOfTypeNotInGraph_Throws()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);

            Assert.Throws<KeyNotFoundException>(() =>
                graph.GetParts(type.Id));
            Assert.Throws<KeyNotFoundException>(() =>
                graph.GetParts(type));
        }

        [Fact]
        public void Created_GetLinksOfTypeNotInGraph_Throws()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);

            Assert.Throws<KeyNotFoundException>(() =>
                graph.GetLinks(type.Id));
            Assert.Throws<KeyNotFoundException>(() =>
                graph.GetLinks(type));
        }

        [Fact]
        public void Created_GetInLinksOfTypeNotInGraph_Throws()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);

            Assert.Throws<KeyNotFoundException>(() =>
                graph.GetInLinks(type.Id));
            Assert.Throws<KeyNotFoundException>(() =>
                graph.GetInLinks(type));
        }

        [Fact]
        public void NonEmpty_GetDerivedTypes_Positive()
        {
            var baseType = CreateSimpleEntityType(typeof(object));
            var type = CreateSimpleEntityType(typeof(string));
            var graph = new TypeGraph(baseType);
            graph.Add(type, baseType);

            var derivedTypes = graph.GetDerivedTypes(baseType);
            Assert.Single(derivedTypes);
            Assert.Equal(type, derivedTypes.FirstOrDefault().Value);
        }

        private EntityType CreateSimpleEntityType(Type type)
        {
            return new EntityType(type, type.FullName, false,
                new Dictionary<string, string>(),
                new Dictionary<string, string>(),
                new Dictionary<string, string>(),
                new Dictionary<string, string>());
        }
    }
}
