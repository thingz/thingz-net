﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common;
using Thingz.Hosting.Instances;
using Thingz.Hosting.Types;
using Xunit;

namespace Thingz.Hosting.Tests.Instances
{
    public class InstanceFactoryTests
    {
        [Fact]
        public void Created_Allocate_Positive()
        {
            var type = CreateSimpleEntityType(typeof(Entity));
            var factory = new InstanceFactory();
            var instance = factory.Allocate(type);

            Assert.NotNull(instance);
            Assert.Equal(type.Implementation, instance.GetType());
        }

        [Fact]
        public void Created_AllocateInvalidType_Throws()
        {
            var type = CreateSimpleEntityType(typeof(object));
            var factory = new InstanceFactory();

            Assert.Throws<ArgumentException>(() =>
                factory.Allocate(type));
        }

        [Fact]
        public void Created_Create_EntityStateChanged()
        {
            var type = CreateSimpleEntityType(typeof(Entity));
            var factory = new InstanceFactory();
            var instance = new Entity();
            var stateBefore = instance.State;
            factory.Create(instance, type);

            Assert.NotEqual(stateBefore, instance.State);
            Assert.NotEqual(EntityState.Allocated, instance.State);
        }

        [Fact]
        public void Created_CreateNonMatchingType_Throws()
        {
            var type = CreateSimpleEntityType(typeof(string));
            var factory = new InstanceFactory();
            var instance = new Entity();

            Assert.Throws<ArgumentException>(() =>
                factory.Create(instance, type));
        }

        private EntityType CreateSimpleEntityType(Type type)
        {
            return new EntityType(type, type.FullName, false,
                new Dictionary<string, string>(),
                new Dictionary<string, string>(),
                new Dictionary<string, string>(),
                new Dictionary<string, string>());
        }
    }
}
