﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common;
using Thingz.Hosting.Instances;
using Thingz.Hosting.Types;
using Xunit;

namespace Thingz.Hosting.Tests.Instances
{
    public class InstanceGraphTests
    {
        [Fact]
        public void Empty_Root_NotNull()
        {
            var type = CreateSimpleEntityType(typeof(Entity));
            var instance = new Entity();
            var graph = new InstanceGraph(instance, type);

            Assert.NotNull(graph.Root);
        }

        [Fact]
        public void Empty_Root_ValueCorrect()
        {
            var type = CreateSimpleEntityType(typeof(Entity));
            var instance = new Entity();
            var graph = new InstanceGraph(instance, type);

            Assert.Equal(instance, graph.Root);
        }

        [Fact]
        public void Empty_GetTypeOfInstance_RootTypeCorrect()
        {
            var type = CreateSimpleEntityType(typeof(Entity));
            var instance = new Entity();
            var graph = new InstanceGraph(instance, type);

            Assert.Equal(type, graph.GetTypeOfInstance(graph.Root));
        }

        [Fact]
        public void Empty_RootNotMatchingRootType_Throws()
        {
            var type = CreateSimpleEntityType(typeof(string));
            var instance = new Entity();

            Assert.Throws<ArgumentException>(() =>
                new InstanceGraph(instance, type));
        }

        private EntityType CreateSimpleEntityType(Type type)
        {
            return new EntityType(type, type.FullName, false,
                new Dictionary<string, string>(),
                new Dictionary<string, string>(),
                new Dictionary<string, string>(),
                new Dictionary<string, string>());
        }
    }
}
