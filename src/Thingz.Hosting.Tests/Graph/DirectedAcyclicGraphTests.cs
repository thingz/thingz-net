﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Hosting.Graph;
using Xunit;

namespace Thingz.Hosting.Tests.Graph
{
    public class DirectedAcyclicGraphTests
    {
        [Fact]
        public void NonEmpty_AddEdgeCausingCircle_Throws()
        {
            var node1 = new object();
            var node2 = new object();
            var graph = new DirectedAcyclicGraph<object>();
            graph.Add("key1", node1);
            graph.Add("key2", node2);
            graph.AddEdge("edge1", node1, node2);

            Assert.Throws<ArgumentException>(() =>
                graph.AddEdge("edge2", node2, node1));
        }

        [Fact]
        public void NonEmpty_CausesCicle_Positive()
        {
            var node1 = new object();
            var node2 = new object();
            var graph = new DirectedAcyclicGraph<object>();
            graph.Add("key1", node1);
            graph.Add("key2", node2);
            graph.AddEdge("edge1", node1, node2);

            Assert.True(graph.CausesCircle(node2, node1));
        }

        [Fact]
        public void NonEmpty_CausesCicle_Negative()
        {
            var node1 = new object();
            var node2 = new object();
            var node3 = new object();
            var graph = new DirectedAcyclicGraph<object>();
            graph.Add("key1", node1);
            graph.Add("key2", node2);
            graph.Add("key3", node3);
            graph.AddEdge("edge1", node1, node2);

            Assert.False(graph.CausesCircle(node2, node3));
        }
    }
}
