﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Hosting.Graph;
using Xunit;

namespace Thingz.Hosting.Tests.Graph
{
    public class DirectedGraphTests
    {
        [Fact]
        public void Created_AddNode_NodeAdded()
        {
            var nodeToAdd = new object();
            var graph = new DirectedGraph<object>();
            var countBefore = graph.Count();
            graph.Add("key", nodeToAdd);
            var countAfter = graph.Count();

            Assert.Equal(countBefore + 1, countAfter);
            Assert.Contains(nodeToAdd, graph);
        }

        [Fact]
        public void Created_AddNode_RaisesEvent()
        {
            var graph = new DirectedGraph<object>();
            bool eventRaised = false;
            graph.NodeAdded += (sender, e) => eventRaised = true;
            graph.Add("key", new object());

            Assert.True(eventRaised);
        }

        [Fact]
        public void Created_AddKeyTwice_Throws()
        {
            var graph = new DirectedGraph<object>();
            graph.Add("key", new object());

            Assert.Throws<ArgumentException>(() =>
                graph.Add("key", new object()));
        }

        [Fact]
        public void Created_AddValueTwice_Throws()
        {
            var graph = new DirectedGraph<object>();
            var node = new object();
            graph.Add("key1", node);

            Assert.Throws<ArgumentException>(() =>
                graph.Add("key2", node));
        }

        [Fact]
        public void NonEmpty_RemoveByKey_NodeRemoved()
        {
            var key = "key";
            var graph = new DirectedGraph<object>();
            graph.Add(key, new object());
            var countBefore = graph.Count();
            graph.Remove(key);
            var countAfter = graph.Count();

            Assert.Equal(countBefore - 1, countAfter);
        }

        [Fact]
        public void NonEmpty_RemoveByKey_RaisesEvent()
        {
            var key = "key";
            var graph = new DirectedGraph<object>();
            graph.Add(key, new object());
            bool eventRaised = false;
            graph.NodeRemoved += (sender, e) => eventRaised = true;
            graph.Remove(key);

            Assert.True(eventRaised);
        }

        [Fact]
        public void NonEmpty_RemoveByInvalidKey_Throws()
        {
            var key1 = "key1";
            var key2 = "key2";
            var graph = new DirectedGraph<object>();
            graph.Add(key1, new object());

            Assert.Throws<KeyNotFoundException>(() =>
                graph.Remove(key2));
        }

        [Fact]
        public void NonEmpty_RemoveByValue_NodeRemoved()
        {
            var node = new object();
            var graph = new DirectedGraph<object>();
            graph.Add("key", node);
            var countBefore = graph.Count();
            graph.Remove(node);
            var countAfter = graph.Count();

            Assert.Equal(countBefore - 1, countAfter);
        }

        [Fact]
        public void NonEmpty_RemoveByValue_RaisesEvent()
        {
            var node = new object();
            var graph = new DirectedGraph<object>();
            graph.Add("key", node);
            bool eventRaised = false;
            graph.NodeRemoved += (sender, e) => eventRaised = true;
            graph.Remove(node);

            Assert.True(eventRaised);
        }

        [Fact]
        public void NonEmpty_RemoveByInvalidValue_Throws()
        {
            var node1 = new object();
            var node2 = new object();
            var graph = new DirectedGraph<object>();
            graph.Add("key", node1);

            Assert.Throws<KeyNotFoundException>(() =>
                graph.Remove(node2));
        }

        [Fact]
        public void Empty_AddEdge_Throws()
        {
            var graph = new DirectedGraph<object>();

            Assert.Throws<ArgumentException>(() =>
                graph.AddEdge("key", new object(), new object()));
        }

        [Fact]
        public void NonEmpty_AddEdgeToInvalidNode_Throws_NoEdgeAdded()
        {
            var graph = new DirectedGraph<object>();
            var key = "key";
            var node1 = new object();
            graph.Add(key, node1);

            Assert.Throws<ArgumentException>(() =>
                graph.AddEdge(key, node1, new object()));

            Assert.Empty(graph.GetOutEdges(node1));
            Assert.Empty(graph.GetInEdges(node1));
        }

        [Fact]
        public void NonEmpty_AddEdgeKeyTwiceForSameSource_Throws()
        {
            var node1 = new object();
            var node2 = new object();
            var graph = new DirectedGraph<object>();
            graph.Add("key1", node1);
            graph.Add("key2", node2);
            graph.AddEdge("key", node1, node2);

            Assert.Throws<ArgumentException>(() =>
                graph.AddEdge("key", node1, node1));
        }

        [Fact]
        public void Empty_RemoveEdge_Throws()
        {
            var graph = new DirectedGraph<object>();

            Assert.Throws<ArgumentException>(() =>
                graph.RemoveEdge("key", new object()));
        }

        [Fact]
        public void NonEmpty_RemoveEdgeByInvalidKey_Throws()
        {
            var graph = new DirectedGraph<object>();
            var node1 = new object();
            var node2 = new object();
            graph.Add("key1", node1);
            graph.Add("key2", node2);
            graph.AddEdge("key", node1, node2);

            Assert.Throws<ArgumentException>(() =>
                graph.RemoveEdge("key3", node1));
        }

        [Fact]
        public void NonEmpty_RemoveEdgeByNodeNotInGraph_Throws()
        {
            var graph = new DirectedGraph<object>();
            var node1 = new object();
            var node2 = new object();
            var key = "key";
            graph.Add("key1", node1);
            graph.Add("key2", node2);
            graph.AddEdge(key, node1, node2);

            Assert.Throws<ArgumentException>(() =>
                graph.RemoveEdge(key, new object()));
        }

        [Fact]
        public void NonEmpty_RemoveEdgeByOtherNodeInGraph_Throws()
        {
            var graph = new DirectedGraph<object>();
            var node1 = new object();
            var node2 = new object();
            var key = "key";
            graph.Add("key1", node1);
            graph.Add("key2", node2);
            graph.AddEdge(key, node1, node2);

            Assert.Throws<ArgumentException>(() =>
                graph.RemoveEdge(key, node2));
        }
    }
}
