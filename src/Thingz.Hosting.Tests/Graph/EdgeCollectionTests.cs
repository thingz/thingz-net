﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Hosting.Graph;
using Xunit;

namespace Thingz.Hosting.Tests.Graph
{
    public class EdgeCollectionTests
    {
        [Fact]
        public void Created_Count_Empty()
        {
            var collection = new EdgeCollection<object>();

            Assert.Empty(collection);
        }

        [Fact]
        public void Created_Add_NodeAdded()
        {
            var collection = new EdgeCollection<object>();
            collection.Add("name", new object(), new object());

            Assert.Single(collection);
        }

        [Fact]
        public void Created_Add_AddsOneNodeExactly()
        {
            var collection = new EdgeCollection<object>();
            var countBefore = collection.Count();
            collection.Add("name", new object(), new object());
            var countAfter = collection.Count();

            Assert.Equal(countBefore + 1, countAfter);
        }

        [Fact]
        public void NonEmpty_Remove_NodeRemoved()
        {
            var collection = new EdgeCollection<object>();
            var source = new object();
            collection.Add("name", source, new object());
            var countBefore = collection.Count();
            var edge = collection.GetBySource(source).First();
            collection.Remove(edge);
            var countAfter = collection.Count();

            Assert.Equal(countBefore - 1, countAfter);
        }

        [Fact]
        public void NonEmpty_Remove_CorrectNodeRemoved()
        {
            var collection = new EdgeCollection<object>();
            var source = new object();
            collection.Add("name", source, new object());
            var edge = collection.GetBySource(source).First();
            collection.Remove(edge);

            Assert.DoesNotContain(edge, collection);
        }

        [Fact]
        public void NonEmpty_GetBySource_ReturnsCorrectEdges()
        {
            var collection = new EdgeCollection<object>();
            var source = new object();
            var target1 = new object();
            var target2 = new object();
            collection.Add("name", source, target1);
            collection.Add("name2", source, target2);
            collection.Add("name2", target1, target2);
            var edges = collection.GetBySource(source);

            Assert.Equal(2, edges.Count());

            Assert.True(edges.Where((edge) =>
                    edge.Source == source &&
                    edge.Target == target1
                ).Any());

            Assert.True(edges.Where((edge) =>
                    edge.Source == source &&
                    edge.Target == target2
                ).Any());

            Assert.False(edges.Where((edge) =>
                    edge.Source == target1 &&
                    edge.Target == target2
                ).Any());
        }

        [Fact]
        public void NonEmpty_GetByTarget_ReturnsCorrectEdges()
        {
            var collection = new EdgeCollection<object>();
            var source1 = new object();
            var source2 = new object();
            var target = new object();
            collection.Add("name", source1, target);
            collection.Add("name2", source2, target);
            collection.Add("name2", source1, source2);
            var edges = collection.GetByTarget(target);

            Assert.Equal(2, edges.Count());

            Assert.True(edges.Where((edge) =>
                    edge.Source == source1 &&
                    edge.Target == target
                ).Any());

            Assert.True(edges.Where((edge) =>
                    edge.Source == source2 &&
                    edge.Target == target
                ).Any());

            Assert.False(edges.Where((edge) =>
                    edge.Source == source1 &&
                    edge.Target == source2
                ).Any());
        }
    }
}
