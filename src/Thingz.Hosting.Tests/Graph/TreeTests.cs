﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Hosting.Graph;
using Xunit;

namespace Thingz.Hosting.Tests.Graph
{
    public class TreeTests
    {
        [Fact]
        public void Empty_Root_NotNull()
        {
            var tree = new Tree<object>("", new object());

            Assert.NotNull(tree.Root);
        }

        [Fact]
        public void Empty_Root_ContainedInNodes()
        {
            var tree = new Tree<object>("", new object());

            Assert.Contains(tree.Root, tree);
        }

        [Fact]
        public void Empty_Constructor_CorrectRoot()
        {
            var node = new object();
            var tree = new Tree<object>("", node);

            Assert.Equal(node, tree.Root);
        }

        [Fact]
        public void Empty_Add_NodeAdded()
        {
            var nodeToAdd = new object();
            var tree = new Tree<object>("", new object());
            var countBefore = tree.Count();
            tree.Add("key", nodeToAdd);
            var countAfter = tree.Count();

            Assert.Equal(countBefore + 1, countAfter);
            Assert.Contains(nodeToAdd, tree);
        }

        [Fact]
        public void Empty_Add_RaisesEvent()
        {
            var tree = new Tree<object>("", new object());
            bool eventRaised = false;
            tree.NodeAdded += (sender, e) => eventRaised = true;
            tree.Add("key", new object());

            Assert.True(eventRaised);
        }

        [Fact]
        public void Empty_AddKeyTwice_Throws()
        {
            var key = "key";
            var tree = new Tree<object>("", new object());
            tree.Add(key, new object());

            Assert.Throws<ArgumentException>(() =>
                tree.Add(key, new object()));
        }

        [Fact]
        public void Empty_AddWithInvalidParent_Throws_NothingAdded()
        {
            var key = "key";
            var tree = new Tree<object>("", new object());

            Assert.Throws<ArgumentException>(() =>
                tree.Add(key, new object(), new object()));

            Assert.Single(tree);
            Assert.Empty(tree.GetChildren(tree.Root));
        }

        [Fact]
        public void Empty_AddValueTwice_Throws()
        {
            var node = new object();
            var tree = new Tree<object>("", new object());
            tree.Add("key1", node);

            Assert.Throws<ArgumentException>(() =>
                tree.Add("key2", node));
        }

        [Fact]
        public void Empty_RemoveRoot_Throws()
        {
            var tree = new Tree<object>("", new object());

            Assert.Throws<ArgumentException>(() =>
                tree.Remove(tree.Root));
        }

        [Fact]
        public void NonEmpty_RemoveSubtree_Throws()
        {
            var tree = new Tree<object>("", new object());
            var key = "key";
            var node = new object();
            tree.Add(key, node);
            tree.Add("key2", new object(), node);

            Assert.Throws<ArgumentException>(() =>
                tree.Remove(key));
        }
    }
}
