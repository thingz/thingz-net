﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thingz.Hosting.Graph
{
    public class Tree<T> : IEnumerable<T>, ICloneable where T : class
    {
        private readonly DirectedAcyclicGraph<T> _graph = new DirectedAcyclicGraph<T>();

		public event EventHandler<T> NodeAdded
		{
			add => _graph.NodeAdded += value;
			remove => _graph.NodeRemoved -= value;
		}
		public event EventHandler<T> NodeRemoved
		{
			add => _graph.NodeRemoved += value;
			remove => _graph.NodeRemoved -= value;
		}

		public T Root { get; }

		protected Tree(Tree<T> tree)
		{
			Add(tree);
			var rootKey = _graph[tree.Root];
			Root = _graph[rootKey];
		}

		public Tree(string rootId, T root)
		{
			Root = root;
			_graph.Add(rootId, root);
		}

		public T this[string key] => _graph[key];
		public string this[T item] => _graph[item];

		public virtual void Add(string key, T value)
		{
			Add(key, value, Root);
		}

		public virtual void Add(string key, T value, T parent)
		{
			try
			{
				_graph.Add(key, value);
				_graph.AddEdge(key, parent, value);
			}
			catch
			{
				if (_graph.Contains(value))
					_graph.Remove(value);
				throw;
			}
		}

		public virtual void Add(Tree<T> subtree)
		{
			_graph.Add(subtree._graph);
		}

		public bool Remove(T value)
		{
			var key = _graph[value];
			return Remove(key);
		}

		public bool Remove(string key)
		{
			if (_graph[key] == Root)
				throw new ArgumentException("Cannot remove root node");
			if (_graph.HasOutEdges(key))
				throw new ArgumentException("Cannot remove nodes with children", nameof(key));
			foreach (var inEdge in _graph.GetInEdges(_graph[key]).ToList())
				_graph.RemoveEdge(_graph.GetOutEdges(inEdge).Where(e => e.Value == _graph[key]).First().Key, inEdge);
			return _graph.Remove(key);
		}

		public IReadOnlyDictionary<string, T> GetChildren(T node)
		{
			return _graph.GetOutEdges(node);
		}

		public T GetParent(T e) => _graph.GetInEdges(e).First();

		public IEnumerator<T> GetEnumerator() => _graph.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => _graph.GetEnumerator();
		object ICloneable.Clone() => Clone(this);

		public static Tree<T> Clone(Tree<T> tree) => new Tree<T>(tree);
	}
}
