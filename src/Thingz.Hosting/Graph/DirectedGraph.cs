﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Common;

namespace Thingz.Hosting.Graph
{
    public class DirectedGraph<T> : IEnumerable<T>, ICloneable
    {
        private readonly BidirectionalDictionary<string, DirectedGraphNode<T>> _nodes = new BidirectionalDictionary<string, DirectedGraphNode<T>>();
        private readonly Dictionary<T, DirectedGraphNode<T>> _nodesByValues = new Dictionary<T, DirectedGraphNode<T>>();

		public event EventHandler<T> NodeAdded;
		public event EventHandler<T> NodeRemoved;

        public DirectedGraph() { }

        protected DirectedGraph(DirectedGraph<T> graph) => Add(graph);

        public T this[string key]
        {
            get => _nodes[key].Value;
            set => _nodes[key] = new DirectedGraphNode<T>(value);
        }

        public string this[DirectedGraphNode<T> node] => _nodes[node];
        public string this[T item] => this[_nodesByValues[item]];

        public virtual void Add(string key, T node) => Add(key, new DirectedGraphNode<T>(node));

        public virtual void Add(string key, DirectedGraphNode<T> node)
        {
            lock (_nodes)
            {
                _nodes.Add(key, node);
                _nodesByValues.Add(node.Value, node);
            }
            NodeAdded?.Invoke(this, node.Value);
		}

		public virtual void Add(DirectedGraph<T> subgraph)
		{
			lock (_nodes)
			{
				foreach (var node in subgraph._nodes)
				{
					DirectedGraphNode<T> newNode;
					if (this.Contains(node.Value.Value))
					{
						newNode = _nodes[node.Key];
					}
					else
					{
						newNode = new DirectedGraphNode<T>(node.Value.Value);
						Add(node.Key, newNode);
					}
				}

				foreach (var node in subgraph._nodes)
				{
					var newNode = _nodes[node.Key];
					foreach (var outEdge in node.Value.OutEdges)
					{
						if (newNode.OutEdges.ContainsKey(outEdge.Key))
							continue;
						AddEdge(outEdge.Key, newNode.Value, outEdge.Value);
					}
				}
			}
		}

		public bool Remove(string key)
		{
			bool result;
			T removedValue;
			lock (_nodes)
			{
				var node = _nodes[key];
				removedValue = node.Value;
				result = _nodes.Remove(key)
					&& _nodesByValues.Remove(node.Value);
			}
			NodeRemoved?.Invoke(this, removedValue);
			return result;
		}

		public bool Remove(T value)
		{
			bool result;
			lock (_nodes)
			{
				if (GetInEdges(value).Any() || GetOutEdges(value).Any())
					throw new ArgumentException("Connected nodes cannot be removed", nameof(value));
				var node = _nodesByValues[value];
				result = _nodes.Remove(node)
					&& _nodesByValues.Remove(value);
			}
			NodeRemoved?.Invoke(this, value);
			return result;
		}

		public virtual void AddEdge(string key, T source, T target)
		{
			lock (_nodes)
			{
				try
				{
					_nodesByValues[source].OutEdges.Add(key, target);
				}
				catch(KeyNotFoundException ex)
				{
					throw new ArgumentException("Source node is not in graph", nameof(source), ex);
				}

				try
				{ 
					_nodesByValues[target].InEdges.Add(source);
				}
				catch (KeyNotFoundException ex)
				{
					_nodesByValues[source].OutEdges.Remove(key);
					throw new ArgumentException("Target node is not in graph", nameof(source), ex);
				}
			}
		}

		public bool RemoveEdge(string key, T source)
		{
			lock (_nodes)
			{
				DirectedGraphNode<T> sourceNode;
				try
				{
					sourceNode = _nodesByValues[source];
				}
				catch (KeyNotFoundException ex)
				{
					throw new ArgumentException("Source node not in graph", nameof(source), ex);
				}

				try
				{
					var target = sourceNode.OutEdges[key];
					var targetNode = _nodesByValues[target];
					return targetNode.InEdges.Remove(sourceNode.Value)
						&& sourceNode.OutEdges.Remove(key);
				}
				catch (KeyNotFoundException ex)
				{
					throw new ArgumentException("Edge does not exist", ex);
				}

			}
		}

		public IReadOnlyDictionary<string, T> GetOutEdges(T node) => _nodesByValues[node].OutEdges;
		public IEnumerable<T> GetInEdges(T node) => _nodesByValues[node].InEdges;
		public bool HasOutEdges(string key) => _nodes[key].OutEdges.Count > 0;

		public IEnumerator<T> GetEnumerator() => _nodesByValues.Keys.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => _nodesByValues.Keys.GetEnumerator();
		object ICloneable.Clone() => Clone(this);
		public static DirectedGraph<T> Clone(DirectedGraph<T> graph) => new DirectedGraph<T>(graph);
	}
}
