﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thingz.Hosting.Graph
{
    public class DirectedAcyclicGraph<T> : DirectedGraph<T>, ICloneable
    {
        private readonly object _lock = new object();

		public DirectedAcyclicGraph() : base()
		{ }

		protected DirectedAcyclicGraph(DirectedAcyclicGraph<T> graph) : base(graph)
		{ }

		public override void Add(DirectedGraph<T> subgraph)
		{
			lock (_lock)
			{
				var duplicateNodes = subgraph.Where(n => this.Contains(n));
				var duplicateEdges = duplicateNodes.SelectMany(n => subgraph.GetOutEdges(n)
					.Where(e => GetOutEdges(n).ContainsKey(e.Key))
					.Select(e => e.Key));

				try
				{
					base.Add(subgraph);
				}
				catch (ArgumentException ex)
				{
					// Reverse adding of subgraph
					foreach (var node in subgraph)
					{
						if (!this.Contains(node))
							continue;
						foreach (var outEdge in subgraph.GetOutEdges(node))
						{
							if (duplicateEdges.Contains(outEdge.Key))
								continue;
							try
							{
								RemoveEdge(outEdge.Key, outEdge.Value);
							}
							catch
							{ }
						}
						if (duplicateNodes.Contains(node))
							continue;
						Remove(node);
					}
					throw new ArgumentException("Subgraph cannot be added.", nameof(subgraph), ex);
				}
			}
		}

		public override void AddEdge(string key, T source, T target)
		{
			lock (_lock)
			{
				if (CausesCircle(source, target))
					throw new ArgumentException("Added edge causes circle.", nameof(target));
				base.AddEdge(key, source, target);
			}
		}

		public bool CausesCircle(T source, T target)
		{
			lock (_lock)
			{
				if (!this.Contains(source) || !this.Contains(target))
					return false;
				if (this[source] == this[target])
					return true;
				var q = new Queue<T>(GetOutEdges(target).Values);
				while (q.Count > 0)
				{
					var current = q.Dequeue();
					if (this[current] == this[source])
						return true;

					foreach (var outEdge in GetOutEdges(current).Values)
					{
						if (this[outEdge] != this[current])
							q.Enqueue(outEdge);
					}
				}
				return false;
			}
		}

		public override string ToString()
		{
			var builder = new StringBuilder();

			builder.Append("DAG<").Append(typeof(T)).AppendLine(">");

			foreach (var node in this)
			{
				builder.AppendLine(node.ToString());
				foreach (var edge in GetOutEdges(node))
				{
					builder.Append('\t').Append(edge.Key).Append(" \t=> ").AppendLine(edge.Value.ToString());
				}
			}

			return builder.ToString();
		}

		object ICloneable.Clone() => new DirectedAcyclicGraph<T>(this);
		public static DirectedAcyclicGraph<T> Clone(DirectedAcyclicGraph<T> graph) => new DirectedAcyclicGraph<T>(graph);
	}
}
