﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thingz.Hosting.Graph
{
    public class EdgeCollection<T> : IEnumerable<Edge<T>>
    {
        private readonly object _lock = new object();

        private readonly List<Edge<T>> _edges = new List<Edge<T>>();
        private readonly Dictionary<T, List<Edge<T>>> _edgesBySource = new Dictionary<T, List<Edge<T>>>();
        private readonly Dictionary<T, List<Edge<T>>> _edgesByTarget = new Dictionary<T, List<Edge<T>>>();

        public void Add(string name, T source, T target)
        {
            lock(_lock)
            {
                var edge = new Edge<T>(name, source, target);
                _edges.Add(edge);
                AddEdgeBySource(edge);
                AddEdgeByTarget(edge);
            }
        }

        private void AddEdgeBySource(Edge<T> edge)
        {
            lock(_lock)
            {
                if (!_edgesBySource.ContainsKey(edge.Source))
                    _edgesBySource.Add(edge.Source, new List<Edge<T>>());

                _edgesBySource[edge.Source].Add(edge);
            }
        }

        private void AddEdgeByTarget(Edge<T> edge)
        {
            lock (_lock)
            {
                if (!_edgesByTarget.ContainsKey(edge.Target))
                    _edgesByTarget.Add(edge.Target, new List<Edge<T>>());

                _edgesByTarget[edge.Target].Add(edge);
            }
        }

        public void Remove(Edge<T> edge)
        {
            lock(_lock)
            {
                if(edge != null && _edges.Contains(edge))
                {
                    _edges.Remove(edge);
                    RemoveBySource(edge);
                    RemoveByTarget(edge);
                }
            }
        }

        private void RemoveBySource(Edge<T> edge)
        {
            lock(_lock)
            {
                _edgesBySource[edge.Source].Remove(edge);
                if (!_edgesBySource[edge.Source].Any())
                    _edgesBySource.Remove(edge.Source);
            }
        }

        private void RemoveByTarget(Edge<T> edge)
        {
            lock(_lock)
            {
                _edgesByTarget[edge.Target].Remove(edge);
                if (!_edgesByTarget[edge.Target].Any())
                    _edgesByTarget.Remove(edge.Target);
            }
        }

        public IEnumerable<Edge<T>> GetBySource(T source)
        {
            lock(_lock)
            {
                if (!_edgesBySource.ContainsKey(source))
                    return Enumerable.Empty<Edge<T>>();

                return _edgesBySource[source];
            }
        }

        public IEnumerable<Edge<T>> GetByTarget(T target)
        {
            lock(_lock)
            {
                if (!_edgesByTarget.ContainsKey(target))
                    return Enumerable.Empty<Edge<T>>();

                return _edgesByTarget[target];
            }
        }

        public IEnumerator<Edge<T>> GetEnumerator() => ((IEnumerable<Edge<T>>)_edges).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable<Edge<T>>)_edges).GetEnumerator();
    }
}
