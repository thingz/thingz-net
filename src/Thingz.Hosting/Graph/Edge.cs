﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Hosting.Graph
{
    public class Edge<T>
    {
        public string Name { get; }
        public T Source { get; }
        public T Target { get; }

        public Edge(string name, T source, T target)
        {
            Name = name;
            Source = source;
            Target = target;
        }
    }
}
