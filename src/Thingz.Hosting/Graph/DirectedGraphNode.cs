﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thingz.Hosting.Graph
{
    public class DirectedGraphNode<T>
    {
        public T Value { get; }

        public Dictionary<string, T> OutEdges { get; } = new Dictionary<string, T>();
        public List<T> InEdges { get; } = new List<T>();

        public DirectedGraphNode(T value)
        {
            Value = value;
        }
    }
}
