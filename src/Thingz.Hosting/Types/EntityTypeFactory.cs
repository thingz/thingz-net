﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Thingz.Common;

namespace Thingz.Hosting.Types
{
    public class EntityTypeFactory
    {
        public EntityType Create(Type rawType)
        {
            var parts = new Dictionary<string, string>();
            var partCollections = new Dictionary<string, string>();
            var links = new Dictionary<string, string>();
            var linkCollections = new Dictionary<string, string>();

            foreach (var property in rawType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (IsReadOnlyPropertyfType<Part>(property))
                    parts.Add(property.Name, GetGenericArgumentId(property));

                else if (IsReadOnlyPropertyfType<PartCollection>(property))
                    partCollections.Add(property.Name, GetGenericArgumentId(property));

                else if (IsReadOnlyPropertyfType<Link>(property))
                    links.Add(property.Name, GetGenericArgumentId(property));

                else if (IsReadOnlyPropertyfType<LinkCollection>(property))
                    linkCollections.Add(property.Name, GetGenericArgumentId(property));
            }

            return new EntityType(rawType, GetTypeId(rawType), rawType.IsAbstract,
                parts, partCollections, links, linkCollections);
        }

        public string GetTypeId(Type rawType) => rawType.FullName;

        private bool IsReadOnlyPropertyfType<T>(PropertyInfo property)
        {
            return property.CanRead && !property.CanWrite &&
                typeof(T).IsAssignableFrom(property.PropertyType);
        }

        private string GetGenericArgumentId(PropertyInfo property)
        {
            return GetTypeId(property.PropertyType.GetGenericArguments().First());
        }
    }
}
