﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Thingz.Hosting.Graph;

namespace Thingz.Hosting.Types
{
    public class TypeResolutionGraph : IEnumerable<EntityType>, ICloneable
    {
        private readonly DirectedAcyclicGraph<EntityType> _graph;

        public EntityType Root { get; }

        public static TypeResolutionGraph Clone(TypeResolutionGraph graph)
        {
            return new TypeResolutionGraph(graph);
        }

        protected TypeResolutionGraph(TypeResolutionGraph graph)
        {
            _graph = DirectedAcyclicGraph<EntityType>.Clone(graph._graph);
            Root = graph.Root;
        }

        public TypeResolutionGraph(EntityType root)
        {
            _graph = new DirectedAcyclicGraph<EntityType>();
            _graph.Add(root.Id, root);
            Root = root;
        }

        public void Add(TypeResolutionGraph subgraph)
        {
            _graph.Add(subgraph._graph);
        }

        public void Add(string linkId, EntityType source, EntityType target)
        {
            _graph.Add(target.Id, target);
            _graph.AddEdge(linkId, source, target);
        }

        object ICloneable.Clone() => new TypeResolutionGraph(this);
        public IEnumerator<EntityType> GetEnumerator() => _graph.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _graph.GetEnumerator();
    }
}
