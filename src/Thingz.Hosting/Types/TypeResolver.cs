﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thingz.Hosting.Types
{
	public class TypeResolver
    {
        private readonly TypeGraph _types;

        public TypeResolver(TypeGraph types)
        {
            _types = types;
		}

		public IEnumerable<EntityType> ReverseResolve(EntityType nodeType)
		{
			foreach (var inlink in _types.GetInLinks(nodeType).ToList())
			{
				var links = _types.GetLinks(inlink);
				if (links.Count() == 1 &&
					links.First().Value == nodeType)
					yield return inlink;
			}
		}

		public IEnumerable<TypeResolutionGraph> FindResolutions(EntityType type)
		{
			var prefix = new TypeResolutionGraph(type);
			return FindResolutions(type, prefix);
		}

		private IEnumerable<TypeResolutionGraph> FindResolutions(EntityType type, TypeResolutionGraph prefix)
		{
			var links = _types.GetLinks(type);
			if (!links.Any())
			{
				if (!type.IsAbstract)
					yield return prefix;
				yield break;
			}

			var linkPaths = links.Select(link =>
				AddLinkResolutions(type, prefix, link));

			foreach (var resolution in MakeResolutions(linkPaths))
				yield return resolution;
		}

		private IEnumerable<TypeResolutionGraph> AddLinkResolutions(EntityType parent, TypeResolutionGraph prefix, KeyValuePair<string, EntityType> link)
		{
			foreach (var linkResult in AddLinkResolutionsForType(parent, prefix, link))
				yield return linkResult;

			foreach (var alternativeLink in _types.GetDerivedTypes(link.Value))
				foreach (var linkResult in AddLinkResolutionsForType(parent, prefix, alternativeLink))
					yield return linkResult;
		}

		private IEnumerable<TypeResolutionGraph> AddLinkResolutionsForType(EntityType parent, TypeResolutionGraph prefix, KeyValuePair<string, EntityType> link)
		{
			if (prefix.Contains(link.Value))
				return Enumerable.Empty<TypeResolutionGraph>();

			var nextPrefix = TypeResolutionGraph.Clone(prefix);
			nextPrefix.Add(link.Key, parent, link.Value);

			return FindResolutions(link.Value, nextPrefix);
		}

		private IEnumerable<TypeResolutionGraph> MakeResolutions(IEnumerable<IEnumerable<TypeResolutionGraph>> linkPaths)
		{
			var linkCombinations = GetPermutations(linkPaths);

			return linkCombinations
				.Where(linkCombination => linkCombination.Any())
				.Select(linkCombination =>
				{
					var result = TypeResolutionGraph.Clone(linkCombination.First());
					foreach (var link in linkCombination.Skip(1))
						result.Add(link);

					return result;
				});
		}

		public static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<IEnumerable<T>> input)
		{
			if (!input.Any())
			{
				yield return Enumerable.Empty<T>();
				yield break;
			}

			var next = GetPermutations(input.Skip(1));
			foreach (var prefix in next)
				foreach (var suffix in input.First())
					yield return prefix.Append(suffix);
		}
	}
}
