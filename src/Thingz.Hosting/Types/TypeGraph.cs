﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Hosting.Graph;

namespace Thingz.Hosting.Types
{
    public class TypeGraph : IEnumerable<EntityType>
    {
        private readonly Tree<EntityType> _hierarchy;
        private readonly DirectedGraph<EntityType> _parts = new DirectedGraph<EntityType>();
        private readonly DirectedGraph<EntityType> _links = new DirectedGraph<EntityType>();

        public EntityType Base => _hierarchy.Root;

        public event EventHandler<EntityType> NodeAdded
        {
            add => _hierarchy.NodeAdded += value;
            remove => _hierarchy.NodeRemoved -= value;
        }
        public event EventHandler<EntityType> NodeRemoved
        {
            add => _hierarchy.NodeRemoved += value;
            remove => _hierarchy.NodeRemoved -= value;
        }

        public TypeGraph(EntityType baseType)
        {
            _hierarchy = new Tree<EntityType>(baseType.Id, baseType);
            _parts.Add(Base.Id, Base);
            _links.Add(Base.Id, Base);
        }

        public void Add(EntityType type, EntityType baseType)
        {
            lock (_hierarchy)
            {
                try
                {
                    _hierarchy.Add(type.Id, type, baseType);
                    _parts.Add(type.Id, type);
                    _links.Add(type.Id, type);
                }
                catch
                {
                    if (_hierarchy.Contains(type))
                    {
                        if (_parts.Contains(type))
                        {
                            if(_links.Contains(type))
                            {
                                _links.Remove(type);
                            }
                            _parts.Remove(type);
                        }
                        _hierarchy.Remove(type);
                    }
                    throw;
                }
            }
        }

        public bool Remove(string id)
        {
            lock (_hierarchy)
            {
                return _hierarchy.Remove(id)
                    && _parts.Remove(id)
                    && _links.Remove(id);
            }
        }

        public void AddPart(string partName, string partTypeId, EntityType parent)
        {
            EntityType partType;
            try
            {
                partType = this[partTypeId];
            }
            catch(KeyNotFoundException ex)
            {
                throw new ArgumentException("Part type is not in graph.", nameof(partTypeId), ex);
            }
            AddPart(partName, partType, parent);
        }

        public void AddPart(string partName, EntityType partType, EntityType parent)
        {
            lock (_hierarchy)
                _parts.AddEdge(partName, parent, partType);
        }

        public void RemovePart(string partName, EntityType parent)
        {
            lock (_hierarchy)
                _parts.RemoveEdge(partName, parent);
        }

        public void AddLink(string linkName, string linkTypeId, EntityType parent)
        {
            EntityType linkType;
            try
            {
                linkType = this[linkTypeId];
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException("Link type is not in graph.", nameof(linkTypeId), ex);
            }
            AddLink(linkName, linkType, parent);
        }

        public void AddLink(string linkName, EntityType linkType, EntityType parent)
        {
            lock (_hierarchy)
                _links.AddEdge(linkName, parent, linkType);
        }

        public void RemoveLink(string linkName, EntityType parent)
        {
            lock (_hierarchy)
                _links.RemoveEdge(linkName, parent);
        }

        public IReadOnlyDictionary<string, EntityType> GetParts(string id)
        {
            var type = this[id];
            return GetParts(type);
        }

        public IReadOnlyDictionary<string, EntityType> GetParts(EntityType type)
        {
            lock (_hierarchy)
                return _parts.GetOutEdges(type);
        }

        public IReadOnlyDictionary<string, EntityType> GetLinks(string id)
        {
            var type = this[id];
            return GetLinks(type);
        }

        public IReadOnlyDictionary<string, EntityType> GetLinks(EntityType type)
        {
            lock (_hierarchy)
                return _links.GetOutEdges(type);
        }

        public IEnumerable<EntityType> GetInLinks(string id)
        {
            var type = this[id];
            return GetInLinks(type);
        }

        public IEnumerable<EntityType> GetInLinks(EntityType type)
        {
            lock (_hierarchy)
                return _links.GetInEdges(type);
        }

        public IEnumerable<KeyValuePair<string, EntityType>> GetDerivedTypes(EntityType type)
        {
            foreach (var child in _hierarchy.GetChildren(type))
            {
                yield return new KeyValuePair<string, EntityType>(
                    child.Key, child.Value);

                foreach (var grandchild in GetDerivedTypes(child.Value))
                    yield return new KeyValuePair<string, EntityType>(
                        grandchild.Key, grandchild.Value);
            }
        }

        public EntityType this[string key] => _hierarchy[key];
        public string this[EntityType type] => _hierarchy[type];

        public IEnumerator<EntityType> GetEnumerator() => _hierarchy.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _hierarchy.GetEnumerator();
    }
}
