﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common;
using Thingz.Common.Internal;

namespace Thingz.Hosting.Types
{
    public class EntityType
    {
        public Type Implementation { get; }
        public string Id { get; }
        public bool IsAbstract { get; }

        public IReadOnlyDictionary<string, string> PartDefinitions { get; }
        public IReadOnlyDictionary<string, string> PartCollectionDefinitions { get; }
        public IReadOnlyDictionary<string, string> LinkDefinitions { get; }
        public IReadOnlyDictionary<string, string> LinkCollectionDefinitions { get; }

        public EntityType(Type implementation, string id, bool isAbstract,
            IReadOnlyDictionary<string, string> partDefinitions,
            IReadOnlyDictionary<string, string> partCollectionDefinitions,
            IReadOnlyDictionary<string, string> linkDefinitions,
            IReadOnlyDictionary<string, string> linkCollectionDefinitions)
        {
            Implementation = implementation;
            Id = id;
            IsAbstract = isAbstract;
            PartDefinitions = partDefinitions;
            PartCollectionDefinitions = partCollectionDefinitions;
            LinkDefinitions = linkDefinitions;
            LinkCollectionDefinitions = linkCollectionDefinitions;
        }

        public bool IsBaseOf(Type type) => Implementation.IsAssignableFrom(type);

        public bool Compare(Type type) => Implementation == type;

        public StaticPartCollection GetParts(object instance) => new StaticPartCollection(GetImplementationPropterties<Part>(instance, PartDefinitions.Keys));

        public StaticLinkCollection GetLinks(object instance) => new StaticLinkCollection(GetImplementationPropterties<Link>(instance, LinkDefinitions.Keys));

        public IDictionary<string, PartCollection> GetPartCollections(object instance) => GetImplementationPropterties<PartCollection>(instance, PartCollectionDefinitions.Keys);

        public IDictionary<string, LinkCollection> GetLinkCollections(object instance) => GetImplementationPropterties<LinkCollection>(instance, LinkCollectionDefinitions.Keys);

        public bool ValidateLinkTarget(string linkId, object target)
        {
            var link = GetLinks(Activator.CreateInstance(Implementation))[linkId];
            return link.Validate(target);
        }

        private IDictionary<string, T> GetImplementationPropterties<T>(object instance, IEnumerable<string> names) where T : class
        {
            ValidateRawType(instance);

            var result = new Dictionary<string, T>();
            foreach (var linkName in names)
            {
                result.Add(linkName, Implementation.GetProperty(linkName).GetValue(instance) as T);
            }

            return result;
        }

        private void ValidateRawType(object instance)
        {
            if (!Implementation.IsAssignableFrom(instance.GetType()))
                throw new ArgumentException("Instance has invalid type.", nameof(instance));
        }

        public override string ToString() => "[ET] " + Id + (IsAbstract ? " {abstact}" : "");
    }
}
