﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Thingz.Hosting.Types
{
    public class Repository : IEnumerable<EntityType>
    {
        private readonly TypeGraph _types;
        private readonly EntityTypeFactory _factory = new EntityTypeFactory();
        private readonly List<Type> _typesToAdd = new List<Type>();
        private readonly TypeResolver _resolver;

        public static Repository Create<TBaseType>()
        {
            return new Repository(typeof(TBaseType));
        }

        public Repository(Type rawBaseType)
        {
            var baseType = _factory.Create(rawBaseType);
            _types = new TypeGraph(baseType);
            _resolver = new TypeResolver(_types);
        }

        public void LoadAssembly<T>()
        {
            LoadAssembly(typeof(T).Assembly);
        }

        public void LoadAssembly(Assembly assembly)
        {
            var types = ValidateTypes(assembly.DefinedTypes);
            CreateTypesInGraph(types);
        }

        public EntityType GetEntityType<T>()
        {
            var typeId = _factory.GetTypeId(typeof(T));
            return _types[typeId];
        }

        public IEnumerable<TypeResolutionGraph> ResolveType(EntityType type)
        {
            return _resolver.FindResolutions(type);
        }

        public IEnumerable<EntityType> ReverseResolve(EntityType type)
        {
            return _resolver.ReverseResolve(type);
        }

        private IEnumerable<Type> ValidateTypes(IEnumerable<TypeInfo> typeInfos)
        {
            foreach (var typeInfo in typeInfos)
            {
                var type = typeInfo.AsType();
                if (ValidateType(type))
                    yield return type;
            }
        }

        private bool ValidateType(Type type)
        {
            return _types.Base.IsBaseOf(type) &&
                !_types.Base.Compare(type);
        }

        private void CreateTypesInGraph(IEnumerable<Type> types)
        {
            // Add requested types to remaining not-added types from last run
            _typesToAdd.AddRange(types);

            bool typesAdded = true;
            while (typesAdded && _typesToAdd.Count > 0)
                typesAdded = TryAddTypes();
        }

        private bool TryAddTypes()
        {
            bool typesAdded = false;
            var q = new Queue<Type>(_typesToAdd);
            _typesToAdd.Clear();

            while(q.Count > 0)
            {
                var type = q.Dequeue();
                typesAdded |= TryAddType(type);
            }
            return typesAdded;
        }

        private bool TryAddType(Type type)
        {
            EntityType entityType = null;
            try
            {
                entityType = _factory.Create(type);
                _types.Add(entityType, _types[type.BaseType.FullName]);
                foreach (var part in entityType.PartDefinitions)
                    _types.AddPart(part.Key, part.Value, entityType);
                foreach (var part in entityType.PartCollectionDefinitions)
                    _types.AddPart(part.Key, part.Value, entityType);
                foreach (var link in entityType.LinkDefinitions)
                    _types.AddLink(link.Key, link.Value, entityType);
                foreach (var link in entityType.LinkCollectionDefinitions)
                    _types.AddLink(link.Key, link.Value, entityType);
                return true;
            }
            catch
            {
                if(entityType != null && _types.Contains(entityType))
                    _types.Remove(entityType.Id);

                // Retry adding type in next round
                _typesToAdd.Add(type);
            }
            return false;
        }

        public EntityType this[string id] => _types[id];

        public IEnumerator<EntityType> GetEnumerator() => _types.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _types.GetEnumerator();
    }
}
