﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Common;
using Thingz.Hosting.Types;

namespace Thingz.Hosting.Instances
{
    public class InstanceResolver
    {
        public IEnumerable<EntityType> ValidateInLinks(Entity instance, IEnumerable<EntityType> candidates)
        {
            return candidates.Where(candidate =>
                candidate.ValidateLinkTarget(candidate.LinkDefinitions.First().Key, instance));
		}
	}
}
