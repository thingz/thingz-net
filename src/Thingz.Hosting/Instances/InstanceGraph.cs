﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Common;
using Thingz.Hosting.Graph;
using Thingz.Hosting.Types;

namespace Thingz.Hosting.Instances
{
    public class InstanceGraph : IEnumerable<Entity>
    {
        private const char PATH_SEPARATOR = '/';

        private readonly Tree<Entity> _hierarchy;
        private readonly DirectedGraph<Entity> _links = new DirectedGraph<Entity>();
        private readonly Dictionary<Entity, EntityType> _types = new Dictionary<Entity, EntityType>();

        public Entity Root => _hierarchy.Root;
        public Entity this[string key] => _hierarchy[key];
        public string this[Entity instance] => _hierarchy[instance];

        public event EventHandler<Entity> NodeAdded
        {
            add => _hierarchy.NodeAdded += value;
            remove => _hierarchy.NodeAdded -= value;
        }

        public event EventHandler<Entity> NodeRemoved
        {
            add => _hierarchy.NodeRemoved += value;
            remove => _hierarchy.NodeRemoved -= value;
        }

        public InstanceGraph(Entity root, EntityType rootType)
        {
            if (!rootType.IsBaseOf(root.GetType()))
                throw new ArgumentException("Type and instance do not match", nameof(rootType));

            _hierarchy = new Tree<Entity>(PATH_SEPARATOR.ToString(), root);
            _links.Add(PATH_SEPARATOR.ToString(), root);
            _types.Add(root, rootType);
        }

        public Entity GetParent(Entity e)
        {
            lock(_hierarchy)
                return _hierarchy.GetParent(e);
        }

        public IReadOnlyDictionary<string, Entity> GetParts(string key)
        {
            lock (_hierarchy)
            {
                var node = _hierarchy[key];
                return GetParts(node);
            }
        }

        public IReadOnlyDictionary<string, Entity> GetParts(Entity node)
        {
            lock (_hierarchy)
            {
                return _hierarchy.GetChildren(node);
            }
        }

        public IReadOnlyDictionary<string, Entity> GetLinks(string key)
        {
            lock (_hierarchy)
            {
                var node = _links[key];
                return GetLinks(node);
            }
        }

        public IReadOnlyDictionary<string, Entity> GetLinks(Entity node)
        {
            lock (_hierarchy)
            {
                return _links.GetOutEdges(node);
            }
        }

        public IEnumerable<Entity> GetInLinks(Entity node)
        {
            lock (_hierarchy)
            {
                return _links.GetInEdges(node);
            }
        }

        public IEnumerable<Entity> GetByType(EntityType type)
        {
            lock (_hierarchy)
            {
                return _types.Where(i => i.Value == type)
                .Select(i => i.Key);
            }
        }

        public EntityType GetTypeOfInstance(Entity instance)
        {
            lock (_hierarchy)
            {
                return _types[instance];
            }
        }

        public void Add(string name, Entity instance, Entity parent, EntityType instanceType)
        {
            lock(_hierarchy)
            {
                var parentKey = _hierarchy[parent];
                var key = parentKey.TrimEnd(PATH_SEPARATOR) + PATH_SEPARATOR + name;
                try
                {
                    _hierarchy.Add(key, instance, parent);
                    _links.Add(key, instance);
                    _types.Add(instance, instanceType);
                }
                catch
                {
                    if(_hierarchy.Contains(instance))
                    {
                        if(_links.Contains(instance))
                        {
                            if(_types.ContainsKey(instance))
                            {
                                _types.Remove(instance);
                            }
                            _links.Remove(instance);
                        }
                        _hierarchy.Contains(instance);
                    }
                }
            }
        }

        public void Remove(string key)
        {
            lock (_hierarchy)
            {
                var instance = _hierarchy[key];
                Remove(instance);
            }
        }

        public void Remove(Entity instance)
        {
            lock(_hierarchy)
            {
                _hierarchy.Remove(instance);
                _links.Remove(instance);
                _types.Remove(instance);
            }
        }

        public void AddLink(Entity instance, string linkName, Entity target)
        {
            lock (_hierarchy)
                _links.AddEdge(linkName, instance, target);
        }

        public void RemoveLink(Entity instance, string linkName)
        {
            lock (_hierarchy)
                _links.RemoveEdge(linkName, instance);
        }

        public IEnumerator<Entity> GetEnumerator() => _hierarchy.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _hierarchy.GetEnumerator();
    }
}
