﻿using System;
using System.Collections.Generic;
using System.Text;
using Thingz.Common;
using Thingz.Common.Internal;
using Thingz.Hosting.Types;

namespace Thingz.Hosting.Instances
{
    public class InstanceFactory
    {
        public Entity Allocate(EntityType type)
        {
            if (!typeof(Entity).IsAssignableFrom(type.Implementation))
                throw new ArgumentException("Type is not derived from Entity.", nameof(type));

            return Activator.CreateInstance(type.Implementation) as Entity;
        }

        public void Create(Entity instance, EntityType type)
        {
            var creationArgs = new EntityCreationArgs(
                type.GetParts(instance),
                type.GetPartCollections(instance),
                type.GetLinks(instance),
                type.GetLinkCollections(instance));
            
            instance.Create(creationArgs);
        }
    }
}
