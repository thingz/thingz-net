﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thingz.Common;
using Thingz.Common.EventsArgs;
using Thingz.Hosting.Types;

namespace Thingz.Hosting.Instances
{
    public class Setup : IEnumerable<Entity>
    {
        private readonly InstanceFactory _factory = new InstanceFactory();
        private readonly InstanceGraph _instances;
        private readonly InstanceResolver _resolver = new InstanceResolver();

        private readonly Repository _repo;
        private readonly EntityType _rootInstanceType;

        public Entity RootEntity { get; }

        public Entity this[string id] => _instances[id];
        public string this[Entity instance] => _instances[instance];

        public event EventHandler<EntityStateChangedArgs> EntityStateChanged;
        public event EventHandler<string> Logged;

        public Setup(EntityType rootInstanceType, Repository repo)
        {
            _repo = repo;
            _rootInstanceType = rootInstanceType;

            RootEntity = _factory.Allocate(_rootInstanceType);
            _instances = new InstanceGraph(RootEntity, _rootInstanceType);
            RegisterNodeEvents(RootEntity);
        }

        public void Start()
        {
            _factory.Create(RootEntity, _rootInstanceType);
            RootEntity.Initialize();
        }

        public EntityType GetTypeOfInstance(string instanceId) => _instances.GetTypeOfInstance(_instances[instanceId]);
        public EntityType GetTypeOfInstance(Entity instance) => _instances.GetTypeOfInstance(instance);

        private void RegisterNodeEvents(Entity node)
        {
            node.PartRequested += Node_PartRequested;
            node.LinkRequested += Node_LinkRequested;
            node.InLinkRequested += Node_InLinkRequested;
            node.LinkChanged += Node_LinkChangedEvent;
            node.EntityStateChanged += Node_EntityStateChanged;
            node.Logged += Node_Logged;
        }

        private void Node_PartRequested(object sender, EntityRequestedArgs e)
        {
            if (!(sender is Entity instance))
                return;

            e.ResolvedEntity = CreateAndAddPart(e.Name, e.TypeId, instance);
        }

        private Entity CreateAndAddPart(string name, string typeId, Entity parent)
        {
            var partType = _repo[typeId];

            var part = _factory.Allocate(partType);
            AddPart(part, name, partType, parent);
            _factory.Create(part, partType);
            return part;
        }

        private void AddPart(Entity part, string name, EntityType partType, Entity parent)
        {
            _instances.Add(name, part, parent, partType);
            RegisterNodeEvents(part);
        }

        private void Node_LinkRequested(object sender, EntityRequestedArgs e)
        {
            var linkType = _repo[e.TypeId];
            var candidates = _instances.GetByType(linkType);

            // Resolve link if there's exactly one candidate
            if (candidates.Count() == 1)
                e.ResolvedEntity = candidates.First();
        }

        private void Node_InLinkRequested(object sender, EntityRequestedArgs e)
        {
            if (!(sender is Entity parent))
                return;

            var targetType = _instances.GetTypeOfInstance(e.ResolvedEntity);
            var candidates = _repo.ReverseResolve(targetType);
            var validatedCandidates = _resolver.ValidateInLinks(e.ResolvedEntity, candidates);

            EntityType type;
            try
            {
                type = validatedCandidates.SingleOrDefault();
            }
            catch (InvalidOperationException ex)
            {
                throw new Exception("Ambigiouos match", ex);
            }
            if (type == null)
                throw new Exception("No candidate found");

            var instance = CreateAndAddPart(e.Name, type.Id, parent);
            type.GetLinks(instance).First().Value.Value = e.ResolvedEntity;
            instance.Start();
            e.ResolvedEntity = instance;
        }

        private void Node_LinkChangedEvent(object sender, TargetChangedArgs e)
        {
            if (!(sender is Entity instance))
                return;
            if (e.NewTarget == e.OldTarget)
                return;

            var oldState = instance.State;

            if (e.OldTarget != null)
            {
                _instances.RemoveLink(instance, e.Name);
                instance.StopThis();
            }
            if (e.NewTarget != null)
                _instances.AddLink(instance, e.Name, e.NewTarget);

            if (oldState == EntityState.Ready)
                instance.Start();
        }

        private void Node_EntityStateChanged(object sender, EntityStateChangedArgs e)
        {
            if (sender is Entity instance && instance != RootEntity)
            {
                if (e.NewState == EntityState.Allocated)
                {
                    _instances.Remove(instance);
                }
                else if (e.NewState == EntityState.Stopping)
                {
                    foreach(var inLink in _instances.GetInLinks(instance))
                    {
                        var link = _instances.GetLinks(inLink).Where(l => l.Value == instance).First();
                        var type = _instances.GetTypeOfInstance(link.Value);
                        type.GetLinks(link.Value)[link.Key].Value = null;
                    }
                }
            }
            EntityStateChanged?.Invoke(sender, e);
        }

        private void Node_Logged(object sender, string e)
        {
            Logged?.Invoke(sender, e);
        }

        public IEnumerator<Entity> GetEnumerator() => _instances.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _instances.GetEnumerator();
    }


    public class Setup<TRoot> : Setup where TRoot : Entity
    {
        public TRoot Root => RootEntity as TRoot;

        public Setup(Repository repo) : base(repo.GetEntityType<TRoot>(), repo)
        { }
    }
}
